"""All urls contained in this file will be appended to the er2_hru_tool URL namespace.

For instance, if a url pattern of the form:

django.conf.urls.url(r'$', views.index)

Is included as a url pattern, the URL "<app_name>/" will be routed to er2_hru_tool.views.index
"""
import django.conf.urls

from er2.apps.er2_hru_tool import views

urlpatterns = [
    django.conf.urls.url(r'api/v1/state/$', views.state),
    django.conf.urls.url(r'^uploadProjectFiles/$', views.upload_project_files),
    django.conf.urls.url(r'^fillDem/$', views.fill_dem),
    django.conf.urls.url(r'^watershedDelineation/$', views.watershed_delineation),
]
