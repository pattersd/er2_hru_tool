/* eslint-disable */
const merge = require('webpack-merge')
const common = require('./webpack.common.js')
// Super cool webpack build dashboard
const DashboardPlugin = require('webpack-dashboard/plugin')

module.exports = merge(common, {
    devtool: 'inline-sourcemap',
    plugins: [
        new DashboardPlugin(),
    ],
    watchOptions: {
        poll: 1000,
    },
})