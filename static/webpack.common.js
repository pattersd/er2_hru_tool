/* eslint-disable */
const debug = process.env.NODE_ENV !== 'production'
const version = process.env.APP_VERSION || 'unknown'
const webpack = require('webpack')
const path = require('path')

module.exports = {
    context: __dirname + '/er2_hru_tool',
    entry: './js/index.js',
    output: {
        path: __dirname + '/er2_hru_tool/dist',
        filename: 'index.min.js',
        sourceMapFilename: 'index.js.map',
    },
    module: {
        loaders: [
            {  // Compile all Javascript using Babel (with React and Flow), targeting the 2 most recent browsers
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            }, {
                // CSS in node_modules is assumed to be global. so don't hash the styles for local scoping
                test: /(node_modules|global).*\.css$/,
                use: [
                    'style-loader',
                    { loader: 'css-loader', options: { importLoaders: 1 } },
                    'postcss-loader'
                ]
            }, {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            }
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            PRODUCTION: JSON.stringify(!debug),
            APP_VERSION: JSON.stringify(version),
        }),
    ],
}