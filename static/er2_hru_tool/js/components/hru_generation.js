// @flow
import React from 'react'
import { css, Stylesheet } from 'aphrodite'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import { withStyles } from 'material-ui/styles'

import { muiStyles } from '../styles/mui'
import RequirementList from './requirement_list'
import AreaSelector from './area_selector'

type PropsType = {
    theme: Stylesheet,
    minimumArea: number,
    minimumAreaUnit: string,
    onChangeMinimumArea: Function,
    onChangeMinimumAreaUnit: Function,
    classes: {
        button: string,
        formControl: string,
    }
}

class HruGeneration extends React.PureComponent<PropsType> {
    handleChange(event) {
        const val = parseFloat(event.target.value)
        if (!(isNaN(val) || val < 0)) {
            this.props.onChangeMinimumArea(val)
        }
    }

    render() {
        const er2Classes = [this.props.theme.controlBar]
        const { classes } = this.props

        return (
            <div className={css(er2Classes)}>
                <div>
                    <TextField
                        id="hru-min-area"
                        label="Minimum Area"
                        value={this.props.minimumArea}
                        onChange={event => this.handleChange(event)}
                        type="number"
                        className={classes.formControl}
                        margin="normal"
                    />
                    <AreaSelector
                        inputId="hru-min-area-unit"
                        inputName="hru-min-area-unit"
                        value={this.props.minimumAreaUnit}
                        onChange={this.props.onChangeMinimumAreaUnit}
                    />
                </div>
                <Button
                    className={classes.button}
                    type="button"
                    onClick={() => console.log('Generating HRUs')}
                    variant="raised"
                    color="primary"
                >
                    Generate HRUs
                </Button>
                <RequirementList
                    headerText="Inputs"
                    requirements={
                        [
                            { id: 'combined', name: 'Combined raster', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
                <RequirementList
                    headerText="Outputs"
                    requirements={
                        [
                            { id: 'hrus', name: 'HRUs raster', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
            </div>
        )
    }
}

export default withStyles(muiStyles)(HruGeneration)
