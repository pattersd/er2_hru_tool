// @flow
import React from 'react'
import { css, Stylesheet } from 'aphrodite'
import RequirementList from './requirement_list'

type PropsType = {
    theme: Stylesheet,
    onDataUpload: Function,
}

class DataUpload extends React.PureComponent<PropsType> {
    constructor(props) {
        super(props)
        this.state = {}
    }

    generateFileChooser(name: String, isRequired: boolean) {
        const theme = this.props.theme
        const lowerName = name.toLowerCase()
        const varName = `${lowerName}File`
        const inputGridArea = `${lowerName}Start / inputStart / ${lowerName}End / inputEnd`
        const fileId = `${lowerName}-file`

        return (
            <div
                key={name}
                style={{
                    gridArea: inputGridArea,
                }}
            >
                <label className={css(theme.label)} htmlFor={fileId}>
                    {name}
                    {isRequired && '*'}
                </label>
                <br />
                <input
                    type="file"
                    className={css(theme.input)}
                    id={fileId}
                    onChange={(event) => {
                        this.setState({
                            [varName]: event.target.files[0],
                        })
                    }}
                />
            </div>
        )
    }

    generateForm(names: String[], required: boolean[]) {
        const theme = this.props.theme
        let templateRows = '['
        const fileChoosers = []

        for (let i = 0; i < names.length; ++i) {
            const lowerName = names[i].toLowerCase()
            templateRows += `${lowerName}Start] 2fr [${lowerName}End `
            fileChoosers.push(this.generateFileChooser(names[i], required[i]))
        }
        templateRows += 'uploadStart] 1fr [uploadEnd]'

        const onUploadClick = (event) => {
            event.preventDefault()

            const files = []
            for (let i = 0; i < names.length; ++i) {
                const name = names[i]
                const lowerName = name.toLowerCase()
                const varName = `${lowerName}File`
                if (this.state[varName]) {
                    files.push({
                        key: lowerName,
                        value: this.state[varName],
                    })
                } else if (required[i]) {
                    alert(`The ${name} file is required`)
                    return
                }
            }

            console.log(files)
            this.props.onDataUpload(files)

            // reset form state after successful upload
            for (let i = 0; i < names.length; ++i) {
                const lowerName = names[i].toLowerCase()
                const varName = `${lowerName}File`
                if (this.state[varName]) {
                    this.setState({
                        [varName]: undefined,
                    })
                }
            }
            this.uploadForm.reset()
        }

        return (
            <form
                style={{
                    display: 'grid',
                    gridTemplateColumns: '[inputStart] 7fr [uploadStart] 3fr [inputEnd uploadEnd]',
                    gridTemplateRows: templateRows,
                    gridColumnGap: '10px',
                    gridRowGap: '10px',
                    margin: '5px',
                }}
                ref={(ref) => {
                    this.uploadForm = ref
                }}
            >
                {fileChoosers}
                <input
                    type="button"
                    value="Upload"
                    className={css(theme.buttonPrimary)}
                    style={{
                        gridArea: 'uploadStart / uploadStart / uploadEnd / uploadEnd',
                    }}
                    onClick={onUploadClick}
                />
            </form>
        )
    }

    render() {
        const theme = this.props.theme
        const classes = [theme.controlBar]

        return (
            <div className={css(classes)}>
                {this.generateForm(
                    ['Outlet', 'DEM', 'Soils', 'Landuse', 'Hydrogeology'],
                    [true, true, false, false, false])
                }
                <RequirementList
                    headerText="Outputs"
                    requirements={
                        [
                            { id: 'outlet', name: 'Outlet vector', toggled: false, disabled: true },
                            { id: 'dem', name: 'DEM Raster', toggled: false, disabled: true },
                            { id: 'soils', name: 'Soils Raster', toggled: false, disabled: true },
                            { id: 'landuse', name: 'Landuse Raster', toggled: false, disabled: true },
                            { id: 'hydrogeology', name: 'Hydrogeology Raster', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
            </div>
        )
    }
}

export default DataUpload
