// @flow
import React from 'react'
import { css, Stylesheet } from 'aphrodite'
import { withStyles } from 'material-ui/styles'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'

import RequirementList from './requirement_list'
import LengthSelector from './length_selector'
import { muiStyles } from '../styles/mui'

type PropsType = {
    theme: Stylesheet,
    demResolution: number,
    demResolutionUnit: String,
    detectedDemResolution: number,
    detectedDemResolutionUnit: string,
    onChangeResolution: Function,
    onChangeResolutionUnit: Function,
    onFillDem: Function,
    classes: {
        button: string,
        formControl: string,
    },
}

class FillDem extends React.PureComponent<PropsType> {
    handleChange(event) {
        const val = parseFloat(event.target.value)
        if (!(isNaN(val) || val < 0)) {
            this.props.onChangeResolution(val)
        }
    }

    render() {
        const er2Classes = [this.props.theme.controlBar]
        const { classes } = this.props

        return (
            <div className={css(er2Classes)}>
                {this.props.detectedDemResolution !== 0 &&
                <p>Detected Resolution: {this.props.detectedDemResolution}{this.props.detectedDemResolutionUnit}</p>
                }
                <div>
                    <TextField
                        id="dem-resolution"
                        label="DEM Resolution"
                        value={this.props.demResolution}
                        onChange={event => this.handleChange(event)}
                        type="number"
                        className={classes.formControl}
                        margin="normal"
                    />
                    <LengthSelector
                        inputId="dem-resolution-unit"
                        inputName="dem-resolution-unit"
                        allowsPixels={false}
                        value={this.props.demResolutionUnit}
                        onChange={this.props.onChangeResolutionUnit}
                    />
                </div>
                <Button
                    className={classes.button}
                    variant="raised"
                    onClick={this.props.onFillDem}
                    color="primary"
                >
                    Fill DEM
                </Button>
                <RequirementList
                    headerText="Inputs"
                    requirements={
                        [
                            { id: 'dem', name: 'DEM Raster', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
                <RequirementList
                    headerText="Outputs"
                    requirements={
                        [
                            { id: 'filled-dem', name: 'Filled DEM Raster', toggled: false, disabled: true },
                            { id: 'flow-direction', name: 'Flow direction raster', toggled: false, disabled: true },
                            {
                                id: 'flow-accumulation',
                                name: 'Flow accumulation Raster',
                                toggled: false,
                                disabled: true,
                            },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
            </div>
        )
    }
}

export default withStyles(muiStyles)(FillDem)
