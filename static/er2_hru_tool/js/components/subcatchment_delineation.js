// @flow
import React from 'react'
import { css, Stylesheet } from 'aphrodite'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import { withStyles } from 'material-ui/styles'

import { muiStyles } from '../styles/mui'
import RequirementList from './requirement_list'
import AreaSelector from './area_selector'

type PropsType = {
    theme: Stylesheet,
    threshold: number,
    thresholdUnit: string,
    onChangeThreshold: Function,
    onChangeThresholdUnit: Function,
    classes: {
        button: string,
        formControl: string,
    }
}

class SubcatchmentDelineation extends React.PureComponent<PropsType> {
    handleChange(event) {
        const val = parseFloat(event.target.value)
        if (!(isNaN(val) || val < 0)) {
            this.props.onChangeThreshold(val)
        }
    }

    render() {
        const er2Classes = [this.props.theme.controlBar]
        const { classes } = this.props

        return (
            <div className={css(er2Classes)}>
                <div>
                    <TextField
                        id="subcatchment-threshold"
                        label="Threshold"
                        value={this.props.threshold}
                        onChange={event => this.handleChange(event)}
                        type="number"
                        className={classes.formControl}
                        margin="normal"
                    />
                    <AreaSelector
                        inputId="subcatchment-threshold-unit"
                        inputName="subcatchment-threshold-unit"
                        value={this.props.thresholdUnit}
                        onChange={this.props.onChangeThresholdUnit}
                    />
                </div>
                <Button
                    className={classes.button}
                    type="button"
                    onClick={() => console.log('Delineating Subcatchments')}
                    variant="raised"
                    color="primary"
                >
                    Delineate Subcatchments
                </Button>
                <RequirementList
                    headerText="Inputs"
                    requirements={
                        [
                            {
                                id: 'cropped-flow-direction',
                                name: 'Flow direction raster',
                                toggled: false,
                                disabled: true,
                            },
                            {
                                id: 'cropped-flow-accumulation',
                                name: 'Flow accumulation Raster',
                                toggled: false,
                                disabled: true,
                            },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
                <RequirementList
                    headerText="Outputs"
                    requirements={
                        [
                            { id: 'subcatchment', name: 'Subcatchment raster', toggled: false, disabled: true },
                            { id: 'reach', name: 'Stream reach raster', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
            </div>
        )
    }
}

export default withStyles(muiStyles)(SubcatchmentDelineation)
