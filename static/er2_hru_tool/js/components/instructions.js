// @flow
import * as React from 'react'
import { withRouter } from 'react-router-dom'

import { TokenNavLink } from 'er2_ui'
import { parseLocation } from 'er2_web_utils'

import * as colors from '../styles/colors'
import * as ids from '../constants/ids'
import * as routes from '../constants/routes'

type Props = {
    location: { pathname: string },
    theme: { button: string, instructions: string, link: string },
    token: { value: string },
}

// Moves a section of documentation into the application viewport based on the url
function scrollIntoView(location: { path: string }) {
    const parsedLoc = parseLocation(location)
    if (parsedLoc.path.startsWith(routes.INSTRUCTIONS_PURPOSE)) {
        document.getElementById(ids.INSTR_PURPOSE_LINK).scrollIntoView()
    } else if (parsedLoc.path.startsWith(routes.INSTRUCTIONS_GETTING_STARTED)) {
        document.getElementById(ids.INSTR_GETTING_STARTED_LINK).scrollIntoView()
    } else if (parsedLoc.path.startsWith(routes.INSTRUCTIONS_ADVANCED)) {
        document.getElementById(ids.INSTR_ADVANCED_LINK).scrollIntoView()
    }
}

/**
 * eslint-disable react/prefer-stateless-function
 * Displays documentation for the application
 */
class Instructions extends React.Component<Props> {
    componentDidMount() {
        scrollIntoView(this.props.location)
    }

    componentDidUpdate() {
        scrollIntoView(this.props.location)
    }

    render() {
        const tokenClasses = `${this.props.theme.button} ${this.props.theme.link}`
        return (
            <div className={this.props.theme.instructions} >
                <TokenNavLink
                    className={tokenClasses}
                    to={routes.INSTRUCTIONS_PURPOSE}
                    token={this.props.token}
                >
                    <h3 id={ids.INSTR_PURPOSE_LINK}>Purpose</h3>
                </TokenNavLink>
                <p>
                    This is the HRU Delineation Tool.
                </p>
                <p>
                    Here is where you will include documentation for your tool.  This page can serve as a starting
                    screen to help your users get familiar with your application, and serve as an easy reference to help
                    them easily remember how to complete different parts of the application.
                </p>
                <TokenNavLink
                    className={tokenClasses}
                    to={routes.INSTRUCTIONS_GETTING_STARTED}
                    token={this.props.token}
                >
                    <h3 id={ids.INSTR_GETTING_STARTED_LINK}>Getting Started</h3>
                </TokenNavLink>
                <p>
                    This documentation can interact with your application.  By including links such as one to&nbsp;
                    <TokenNavLink
                        className={tokenClasses}
                        role="button"
                        to={routes.SELECT_STATION}
                        token={this.props.token}
                    >
                        Select a Station
                    </TokenNavLink>
                    &nbsp;you can easily navigate to different parts of your application without reloading the page, and
                    your browser history will make it easy for the user to return to the documentation if needed.
                </p>
                <p>
                    Documentation is organized into three sections, <q>Purpose,</q> <q>Getting Started,</q> and
                    <q>Advanced Users.</q>  The Purpose should provide a brief overview that explains why the tool exists
                    and who it is designed to help.  The Getting Started section will navigate the user step-by-step through
                    a basic use-case of the application.  Any advanced use cases, or features not immediately needed for
                    most users, can go in the Advanced section.  Organizing your documentation this way will give your
                    users a consistent way to use all of your Catena applications.
                </p>
                <p>
                    That said, feel free to organize your documentation in the most helpful way for your own application.
                </p>
                <TokenNavLink
                    className={tokenClasses}
                    to={routes.INSTRUCTIONS_ADVANCED}
                    token={this.props.token}
                >
                    <h3 id={ids.INSTR_ADVANCED_LINK}>Advanced</h3>
                </TokenNavLink>
                <p>
                    Each section of documentation is separated by a subheader (such as the one above), which is linked
                    so that it will be easy for users to navigate back to that section easily and share it with others.
                </p>
                <p>
                    All of this documentation is rendered using&nbsp;
                    <a className={this.props.theme.link} href="https://facebook.github.io/jsx/" >
                        JSX markup
                    </a>, which is a superset of HTML.  Because of
                    this, it is really easy to <span style={{ color: colors.secondaryOne }}>style your text</span> or insert screenshots
                    to fit your needs:
                    <img src={'/static/er2_hru_tool/img/screenshot.png'} style={{ margin: '10px 0', width: '100%' }} />
                </p>
            </div>
        )
    }
}

export default withRouter(Instructions)