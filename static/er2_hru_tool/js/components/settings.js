// @flow
import React from 'react'
import { css } from 'aphrodite'

export type SettingsType = {
    theme: {
        controlBar: string,
        controlBarTop: string,
    }
}

export default function Settings(props: SettingsType) {
    const rootClassNames = css(props.theme.controlBar)
    return (
        <div className={rootClassNames}>
            <div className={css(props.theme.controlBarTop)}>
                Settings will go here.
            </div>
        </div>
    )
}
