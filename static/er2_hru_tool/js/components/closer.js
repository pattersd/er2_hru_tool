// @flow
import * as React from 'react'
import { css } from 'aphrodite/no-important'
import { styles } from '../styles/root'

type Props = {
    isOpen: boolean,
    moveLeft?: boolean,
    onToggle: Function,
    settings: SettingsBarType,
    type: 'contextBar' | 'south' | 'header',
}

function Closer(props: Props) {
    const rootClassNames = []
    let closerClass
    if (props.type === 'south') {
        rootClassNames.push(styles.closerHorizontal)
        if (props.moveLeft) rootClassNames.push(styles.startLeft)

        if (props.isOpen) {
            closerClass = 'fa fa-caret-down'
        } else {
            rootClassNames.push(styles.startDown)
            closerClass = 'fa fa-caret-up'
        }
    } else if (props.type === 'contextBar') {
        rootClassNames.push(styles.closerVertical)
        if (props.isOpen) {
            closerClass = 'fa fa-caret-left'
        } else {
            closerClass = 'fa fa-caret-right'
            rootClassNames.push(styles.startLeft)
        }
    } else if (props.type === 'header') {
        rootClassNames.push(styles.closerVertical, styles.closerVerticalRight)
        if (props.isOpen) {
            closerClass = 'fa fa-caret-up'
        } else {
            closerClass = 'fa fa-caret-down'
            rootClassNames.push(styles.startUp)
        }
    }

    return (
        <div
            role="button"
            tabIndex="-1"
            className={css(rootClassNames)}
            onClick={() => props.onToggle(!props.isOpen)}
        >
            <span className={closerClass} />
        </div>
    )
}

Closer.defaultProps = {
    moveLeft: false,
}

export default Closer
