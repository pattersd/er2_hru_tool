// @flow
import React from 'react'
import { css } from 'aphrodite'
import { styles } from '../styles/root'

export type LoggerProps = {
    log: string[],
    tocVisible: boolean,
}

class Logger extends React.Component<LoggerProps> {
    constructor(props) {
        super(props)
    }

    render() {
        const rootClassNames = [styles.analysisPanel]
        if (!this.props.tocVisible) rootClassNames.push(styles.moveLeft)

        return (
            <div className={css(rootClassNames)}>
                <div className={css(styles.controlBarTop)} ref={(c) => { this.container = c }}>
                    <pre style={{ scroll: 'auto' }}>
                        {this.props.log.join('\r\n')}
                    </pre>
                </div>
            </div>
        )
    }
}

export default Logger
