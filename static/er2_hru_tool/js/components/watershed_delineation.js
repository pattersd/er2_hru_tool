// @flow
import React from 'react'
import { css, Stylesheet } from 'aphrodite'
import Button from 'material-ui/Button'
import TextField from 'material-ui/TextField'
import { withStyles } from 'material-ui/styles'

import { muiStyles } from '../styles/mui'
import RequirementList from './requirement_list'
import LengthSelector from './length_selector'

type PropsType = {
    theme: Stylesheet,
    snapDistance: number,
    snapDistanceUnit: string,
    onChangeSnapDistance: Function,
    onChangeSnapDistanceUnit: Function,
    onWatershedDelineation: Function,
    classes: {
        button: string,
        formControl: string,
    },
}

class WatershedDelineation extends React.PureComponent<PropsType> {
    handleChange(event) {
        const val = parseFloat(event.target.value)
        if (!(isNaN(val) || val < 0)) {
            this.props.onChangeSnapDistance(val)
        }
    }

    render() {
        const er2Classes = [this.props.theme.controlBar]
        const { classes } = this.props

        return (
            <div className={css(er2Classes)}>
                <div>
                    <TextField
                        id="snap-distance"
                        label="Snap Distance"
                        value={this.props.snapDistance}
                        onChange={event => this.handleChange(event)}
                        type="number"
                        className={classes.formControl}
                        margin="normal"
                    />
                    <LengthSelector
                        inputId="snap-distance-unit"
                        inputName="snap-distance-unit"
                        allowsPixels
                        value={this.props.snapDistanceUnit}
                        onChange={this.props.onChangeSnapDistanceUnit}
                    />
                </div>
                <Button
                    className={classes.button}
                    type="button"
                    onClick={this.props.onWatershedDelineation}
                    variant="raised"
                    color="primary"
                >
                    Delineate Watershed
                </Button>
                <RequirementList
                    headerText="Inputs"
                    requirements={
                        [
                            {
                                id: 'flow-accumulation',
                                name: 'Flow accumulation Raster',
                                toggled: false,
                                disabled: true,
                            },
                            { id: 'outlet', name: 'Outlet vector', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
                <RequirementList
                    headerText="Outputs"
                    requirements={
                        [
                            { id: 'watershed', name: 'Watershed raster', toggled: false, disabled: true },
                            { id: 'slope', name: 'Slope raster', toggled: false, disabled: true },
                            { id: 'aspect', name: 'Aspect raster', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
            </div>
        )
    }
}

export default withStyles(muiStyles)(WatershedDelineation)
