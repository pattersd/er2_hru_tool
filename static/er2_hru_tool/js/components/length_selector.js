// @flow
import React from 'react'
import Select from 'material-ui/Select'
import { MenuItem } from 'material-ui/Menu'
import { FormControl } from 'material-ui/Form'
import { withStyles } from 'material-ui/styles'

import { muiStyles } from '../styles/mui'

type Props = {
    inputId: string,
    inputName: string,
    value: string,
    onChange: Function,
    allowsPixels?: boolean,
    classes: {
        formControl: string,
    },
}

class LengthSelector extends React.Component<Props> {
    handleChange(event) {
        this.props.onChange(event.target.value)
    }

    render() {
        const { classes } = this.props

        return (
            <FormControl className={classes.formControl}>
                <Select
                    inputProps={{
                        id: this.props.inputId,
                        name: this.props.inputName,
                    }}
                    value={this.props.value}
                    onChange={event => this.handleChange(event)}
                >
                    <MenuItem value="m">m</MenuItem>
                    <MenuItem value="km">km</MenuItem>
                    <MenuItem value="ft">ft</MenuItem>
                    {this.props.allowsPixels &&
                    <MenuItem value="px">pixels</MenuItem>
                    }
                </Select>
            </FormControl>
        )
    }
}

export default withStyles(muiStyles)(LengthSelector)
