// @flow
import React from 'react'
import { css, Stylesheet } from 'aphrodite'
import Button from 'material-ui/Button'
import List, {
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    ListSubheader,
} from 'material-ui/List'
import Switch from 'material-ui/Switch'
import { withStyles } from 'material-ui/styles'

import { muiStyles } from '../styles/mui'
import RequirementList from './requirement_list'

type PropsType = {
    theme: Stylesheet,
    overlays: Map,
    onChangeOverlays: Function,
    classes: {
        button: string,
    }
}

class DataOverlay extends React.PureComponent<PropsType> {
    handleChange(event) {
        const target = event.target
        this.props.onChangeOverlays(target.name, target.checked)
    }

    generateOverlay(name: String) {
        return (
            <ListItem key={name}>
                <ListItemText primary={name} />
                <ListItemSecondaryAction>
                    <Switch
                        name={name}
                        checked={this.props.overlays.get(name)}
                        onChange={event => this.handleChange(event)}
                    />
                </ListItemSecondaryAction>
            </ListItem>
        )
    }

    generateOverlays(names: String[]) {
        const overlays = []

        for (let i = 0; i < names.length; ++i) {
            overlays.push(this.generateOverlay(names[i]))
        }

        return (
            <List subheader={<ListSubheader>Layers</ListSubheader>}>
                {overlays}
            </List>
        )
    }

    render() {
        const er2Classes = [this.props.theme.controlBar]
        const { classes } = this.props

        return (
            <div className={css(er2Classes)}>
                {this.generateOverlays(['Soils', 'Landuse', 'Hydrogeology', 'Slope', 'Aspect'])}
                <Button
                    className={classes.button}
                    type="button"
                    onClick={() => console.log('Overlaying')}
                    variant="raised"
                    color="primary"
                >
                    Overlay
                </Button>
                <RequirementList
                    headerText="Inputs"
                    requirements={
                        [
                            { id: 'cropped-dem', name: 'DEM raster', toggled: false, disabled: true },
                            { id: 'cropped-soils', name: 'Soils raster', toggled: false, disabled: true },
                            { id: 'cropped-landuse', name: 'Landuse raster', toggled: false, disabled: true },
                            { id: 'cropped-hydrogeology', name: 'Hydrogeology raster', toggled: false, disabled: true },
                            { id: 'slope', name: 'Slope raster', toggled: false, disabled: true },
                            { id: 'aspect', name: 'Aspect raster', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
                <RequirementList
                    headerText="Outputs"
                    requirements={
                        [
                            { id: 'combined', name: 'Combined raster', toggled: false, disabled: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
            </div>
        )
    }
}

export default withStyles(muiStyles)(DataOverlay)
