// @flow
import React from 'react'
import { css, Stylesheet } from 'aphrodite'
import Button from 'material-ui/Button'
import Select from 'material-ui/Select'
import { MenuItem } from 'material-ui/Menu'
import { FormControl } from 'material-ui/Form'
import { withStyles } from 'material-ui/styles'

import { muiStyles } from '../styles/mui'
import RequirementList from './requirement_list'

type PropsType = {
    theme: Stylesheet,
    inputModel: string,
    onChangeInputModel: Function,
    classes: {
        button: string,
        formControl: string,
    }
}

class InputGeneration extends React.PureComponent<PropsType> {
    handleChange(event) {
        this.props.onChangeInputModel(event.target.value)
    }

    render() {
        const er2Classes = [this.props.theme.controlBar]
        const { classes } = this.props

        return (
            <div className={css(er2Classes)}>
                <FormControl className={classes.formControl}>
                    <Select value={this.props.inputModel} onChange={event => this.handleChange(event)}>
                        <MenuItem value="ages">AgES</MenuItem>
                    </Select>
                </FormControl>
                <Button
                    className={classes.button}
                    type="button"
                    onClick={() => console.log('Generating files')}
                    variant="raised"
                    color="primary"
                >
                    Generate Files
                </Button>
                <RequirementList
                    headerText="Inputs"
                    requirements={
                        [
                            { id: 'hrus', name: 'HRUs raster', toggled: false, disabled: true },
                            { id: 'reach', name: 'Stream reach raster', toggled: false, disabled: true },
                            { id: 'topology', name: 'Topology', toggled: false, disabled: true, disableSwitch: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
                <RequirementList
                    headerText="Outputs"
                    requirements={
                        [
                            {
                                id: 'hru-input',
                                name: 'HRU input file',
                                toggled: false,
                                disabled: true,
                                disableSwitch: true,
                            },
                            {
                                id: 'reach-input',
                                name: 'Reach input file',
                                toggled: false,
                                disabled: true,
                                disableSwitch: true,
                            },
                            {
                                id: 'topology-input',
                                name: 'Topology input file',
                                toggled: false,
                                disabled: true,
                                disableSwitch: true,
                            },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
            </div>
        )
    }
}

export default withStyles(muiStyles)(InputGeneration)
