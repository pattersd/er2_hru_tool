// @flow
import React from 'react'
import { css, Stylesheet } from 'aphrodite'
import Button from 'material-ui/Button'
import List, {
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
} from 'material-ui/List'
import Switch from 'material-ui/Switch'
import { withStyles } from 'material-ui/styles'

import { muiStyles } from '../styles/mui'
import RequirementList from './requirement_list'

type PropsType = {
    theme: Stylesheet,
    dissolveCycles: boolean,
    onChangeDissolveCycles: Function,
    classes: {
        button: string,
    }
}

class TopologyGeneration extends React.PureComponent<PropsType> {
    handleChange(event) {
        this.props.onChangeDissolveCycles(event.target.checked)
    }

    render() {
        const er2Classes = [this.props.theme.controlBar]
        const { classes } = this.props

        return (
            <div className={css(er2Classes)}>
                <List>
                    <ListItem>
                        <ListItemText primary="Dissolve Cycles" />
                        <ListItemSecondaryAction>
                            <Switch
                                checked={this.props.dissolveCycles}
                                onChange={event => this.handleChange(event)}
                            />
                        </ListItemSecondaryAction>
                    </ListItem>
                </List>
                <Button
                    className={classes.button}
                    type="button"
                    onClick={() => console.log('Generating Topology')}
                    variant="raised"
                    color="primary"
                >
                    Generate Topology
                </Button>
                <RequirementList
                    headerText="Inputs"
                    requirements={
                        [
                            { id: 'hrus', name: 'HRUs raster', toggled: false, disabled: true },
                            {
                                id: 'cropped-flow-direction',
                                name: 'Flow direction raster',
                                toggled: false,
                                disabled: true,
                            },
                            {
                                id: 'cropped-flow-accumulation',
                                name: 'Flow accumulation raster',
                                toggled: false,
                                disabled: true,
                            },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
                <RequirementList
                    headerText="Outputs"
                    requirements={
                        [
                            { id: 'topology', name: 'Topology', toggled: false, disabled: true, disableSwitch: true },
                        ]
                    }
                    onToggle={() => id => console.log(id)}
                />
            </div>
        )
    }
}

export default withStyles(muiStyles)(TopologyGeneration)
