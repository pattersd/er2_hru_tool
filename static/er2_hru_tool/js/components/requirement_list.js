// @flow
import React from 'react'
import List, {
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    ListSubheader,
} from 'material-ui/List'
import Switch from 'material-ui/Switch'
import { css } from 'aphrodite'

type Requirement = {
    id: string,
    name: string,
    toggled: boolean,
    disabled: boolean,
    disableSwitch: ?boolean,
}

type PropsType = {
    headerText: string,
    requirements: Requirement[],
    onToggle: Function,
}

class RequirementList extends React.PureComponent<PropsType> {
    generateListItem(req: Requirement) {
        return (
            <ListItem key={req.id} disabled={req.disabled}>
                <ListItemText primary={req.name} />
                <ListItemSecondaryAction>
                    {!('disableSwitch' in req && req.disableSwitch) &&
                    <Switch
                        disabled={req.disabled}
                        checked={req.toggled}
                        onChange={this.handleToggle(req)}
                    />
                    }
                </ListItemSecondaryAction>
            </ListItem>
        )
    }

    generateList() {
        const requirements = this.props.requirements
        const listItems = []

        for (let i = 0; i < requirements.length; ++i) {
            listItems.push(this.generateListItem(requirements[i]))
        }

        return (
            <List subheader={<ListSubheader>{this.props.headerText}</ListSubheader>}>
                {listItems}
            </List>
        )
    }

    handleToggle(req: Requirement) {
        this.props.onToggle(req.id)
    }

    render() {
        return (
            this.generateList()
        )
    }
}

export default RequirementList
