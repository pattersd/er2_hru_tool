// @flow
import React from 'react'
import Select from 'material-ui/Select'
import { MenuItem } from 'material-ui/Menu'
import { FormControl } from 'material-ui/Form'
import { withStyles } from 'material-ui/styles'

import { muiStyles } from '../styles/mui'

type Props = {
    inputId: string,
    inputName: string,
    value: string,
    onChange: Function,
    classes: {
        formControl: string,
    }
}

class AreaSelector extends React.Component<Props> {
    handleChange(event) {
        this.props.onChange(event.target.value)
    }

    render() {
        const { classes } = this.props

        return (
            <FormControl className={classes.formControl}>
                <Select
                    inputProps={{
                        name: this.props.inputName,
                        id: this.props.inputId,
                    }}
                    value={this.props.value}
                    onChange={event => this.handleChange(event)}
                >
                    <MenuItem value="m2">m&sup2;</MenuItem>
                    <MenuItem value="km2">km&sup2;</MenuItem>
                    <MenuItem value="ft2">ft&sup2;</MenuItem>
                    <MenuItem value="px">pixels</MenuItem>
                    <MenuItem value="acre">acre</MenuItem>
                    <MenuItem value="ha">ha</MenuItem>
                </Select>
            </FormControl>
        )
    }
}

export default withStyles(muiStyles)(AreaSelector)
