// @flow
import React from 'react'
import Map from 'er2-map'
import { css } from 'aphrodite'
import type { UserMapsourceType } from 'er2-map-userlayers'
import { styles } from '../styles/hru_tool_map_style'

type ViewType = {
    center: [number, number],
    resolution: number,
    rotation: number,
}
type Props = {
    baseLayer: string,
    updateCurrentView: Function,
    mapsources: Array<UserMapsourceType>,
    moveLeft: boolean,
    moveDown: boolean,
    theme: {
        mapCtr: string,
        moveDown: string,
        moveLeft: string,
    },
}

class HruToolMap extends React.Component<Props> {
    static defaultProps = {
        updateCurrentView: (ex: ViewType) => ex,
    }
    mapApp: any

    render() {
        const classes = [this.props.theme.mapCtr]
        if (this.props.moveLeft) {
            classes.push(styles.moveLeft)
        }
        if (this.props.moveDown) {
            classes.push(styles.moveDown)
        }

        return (
            <div
                className={css(classes)}
            >
                <Map
                    {...this.props}
                    onChangeView={ex => this.props.updateCurrentView(ex)}
                    ref={(mapApp) => {
                        this.mapApp = mapApp
                    }}
                />
            </div>
        )
    }
}

export default HruToolMap
