/**
 * Used as a shorthand for inserting icons.  Helpful especially
 * when you'd like to use the same icon to represent something
 * in multiple areas of the application.  This way, you only have to
 * change the icon once here, and changes are reflected throughout the
 * application.
 */
export const LOAD_ESTIMATION = 'fa fa-dashboard'