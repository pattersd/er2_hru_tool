/**
 * Contains ids assigned to elements in the DOM.  Useful to store these
 * here because the ids are used in actions and reducers to determine
 * the source of redux actions and ui interactions.
 */
export const DATA_OVERLAY = 'overlay'
export const DATA_UPLOAD = 'data-upload'
export const DOCUMENTATION_LINK = 'documentationLink'
export const FILL_DEM = 'fill-dem'
export const HRU_GENERATION = 'hru-generation'
export const INPUT_GENERATION = 'input-generation'
export const INSTR_PURPOSE_LINK = 'purpose'
export const INSTR_GETTING_STARTED_LINK = 'getting-started'
export const INSTR_ADVANCED_LINK = 'advanced'
export const SELECT_BASE_LAYERS = 'select-base-layers'
export const SETTINGS = 'settings'
export const SUBCATCHMENT_DELINEATION = 'subcatchment-delineation'
export const TOPOLOGY_GENERATION = 'topology-generation'
export const USER_LAYERS = 'user-layers'
export const WATERSHED_DELINEATION = 'watershed-delineation'
