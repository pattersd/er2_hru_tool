// @flow
import 'date-input-polyfill'
import Raven from 'raven-js'
import createRavenMiddleware from 'raven-for-redux'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { applyMiddleware, compose, createStore } from 'redux'
import thunkMiddleware from 'redux-thunk'

import App from './containers/app'
import reducer from './reducers'
import '../../node_modules/rc-tree/assets/index.css'

declare var APP_VERSION:string
declare var PRODUCTION:string

let store
// Raven middleware must be injected before all other middleware
const ravenMiddleware = createRavenMiddleware(Raven)
if (PRODUCTION) {
    store = createStore(reducer, applyMiddleware(ravenMiddleware, thunkMiddleware))
    // Configure Raven for sentry.io client-side error reporting
    // https://docs.sentry.io/clients/javascript/install/
    Raven.config('https://b7c08389d2f84abcaa2b0a27560c2443@sentry.io/245771', {
        release: `${PRODUCTION ? APP_VERSION : 'development'}`,
    }).install()
} else {
    const middleware = [ravenMiddleware, thunkMiddleware]
    /* eslint-disable no-underscore-dangle */
    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
        // Enable Redux Debugger: http://extension.remotedev.io/#usage
        const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
        const enhancer = composeEnhancers(applyMiddleware(...middleware))
        store = createStore(reducer, enhancer)
    } else {
        store = createStore(reducer, applyMiddleware(...middleware))
    }
}

render(
    <Provider store={store}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root'),
)
