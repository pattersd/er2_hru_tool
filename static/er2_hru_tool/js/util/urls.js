// @flow
import { parse } from 'query-string'

export function parseLocation(location: { pathname: string, search?: string }): { path: string, params: {} } {
    return { path: location.pathname, params: parse(location.search) || {} }
}
