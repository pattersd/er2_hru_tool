// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type DataUploadType = {
}

type StateType = {visible: boolean, ...DataUploadType}

type ActionType = {
    type: string,
    id: string,
    ...DataUploadType,
}

const initialState = {
    visible: true,
}

export default function dataUpload(state: StateType = initialState, action: ActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        // TODO: update this
        if (action.id === ids.DATA_UPLOAD) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state
    default:
        return state
    }
}
