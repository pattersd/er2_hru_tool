// @flow
import {CHANGE_BASELAYER, CLICK_MAP_TOOLBAR_ITEM} from '../constants/action_types'
import * as ids from '../constants/ids'

export type PanelType = {
    visible: boolean,
}

export type MapToolbarItemType = {
    active: boolean,
    icon: { default: string },
    id: string,
    name: string,
}

export type MapToolbarType = {
    panel: PanelType,
    items: MapToolbarItemType[],
}

type MapToolbarActionType = {
    type: string,
    id: string,
    active?: boolean,
}

const initialState = {
    isContracted: false,
    items: [{
        active: false,
        icon: { default: 'ms ms-layers-base' },
        id: ids.SELECT_BASE_LAYERS,
        name: 'Map Layers',
    }, {
        active: false,
        icon: { default: 'ms ms-layers-add' },
        id: ids.USER_LAYERS,
        name: 'User Layers',
    }],
    panel: { visible: false },
}

function isPanelVisible(items, action) {
    let isVisible = false
    items.find((item) => {
        if (item.id === action.id && !item.active) {
            isVisible = true
            return true
        }
        return false
    })
    return isVisible
}

export default function mapToolbar(state: MapToolbarType = initialState, action: MapToolbarActionType) {
    switch (action.type) {
    case CLICK_MAP_TOOLBAR_ITEM:
        return {
            ...state,
            panel: { visible: isPanelVisible(state.items, action) },
            items: state.items.map(item => (
                item.id === action.id ?
                    { ...item, active: !item.active } :
                    { ...item, active: false }
            )),
        }

    default:
        return state
    }
}
