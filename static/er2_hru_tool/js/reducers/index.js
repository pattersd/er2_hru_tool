// Combines all reducers for the application into a single exportable object
import { combineReducers } from 'redux'

import map from 'er2-map-userlayers/js/reducers/map'
import dataUpload from './data_upload'
import fillDem from './fill_dem'
import watershedDelineation from './watershed_delineation'
import subcatchmentDelineation from './subcatchment_delineation'
import dataOverlay from './data_overlay'
import hruGeneration from './hru_generation'
import topologyGeneration from './topology_generation'
import inputGeneration from './input_generation'
import logger from './logger'
import contextBar from './context_bar'
import tableOfContents from './table_of_contents'
import mapToolbar from './map_toolbar'
import meta from './meta'
import serverState from './server_state'
import settings from './settings'
import token from './token'

const rootReducer = combineReducers({
    dataUpload,
    fillDem,
    watershedDelineation,
    subcatchmentDelineation,
    dataOverlay,
    hruGeneration,
    topologyGeneration,
    inputGeneration,
    logger,
    contextBar,
    map,
    mapToolbar,
    meta,
    serverState,
    settings,
    tableOfContents,
    token,
})

export default rootReducer
