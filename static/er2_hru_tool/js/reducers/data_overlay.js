// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type DataOverlayType = {
    overlays: Map,
}

type StateType = { visible: boolean, ...DataOverlayType }

type ActionType = {
    type: string,
    id: string,
    name: string,
    checked: boolean,
}

const initialState = {
    visible: true,
    overlays: new Map([['Soils', false],
        ['Landuse', false], ['Hydrogeology', false], ['Slope', false], ['Aspect', false]]),
}

export default function dataOverlay(state: StateType = initialState, action: ActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        // TODO: update this
        if (action.id === ids.FILL_DEM) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state
    case actionTypes.CHANGE_OVERLAY: {
        const m = new Map(state.overlays)
        m.set(action.name, action.checked)
        return { ...state, overlays: m }
    }
    default:
        return state
    }
}
