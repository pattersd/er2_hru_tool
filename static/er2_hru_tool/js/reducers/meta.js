// Manages state for metadata for the application
import * as actionTypes from '../constants/action_types'

const initialState = {
    name: 'er2_hru_tool',
    version: 'undefined',
}

export default function meta(state = initialState, action) {
    switch (action.type) {
    case actionTypes.SET_FETCHED_STATE:
        return {
            ...state,
            version: action.meta.version,
        }

    default:
        return state
    }
}
