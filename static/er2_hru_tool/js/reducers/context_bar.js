// @flow
// Manages state for all actions related to the Context Bar
import React from 'react'
import { CLICK_CONTEXT_BAR_ITEM } from '../constants/action_types'
import * as ids from '../constants/ids'
import * as routes from '../constants/routes'

/**
 * Returns true if the url matches the item's link.
 *
 * If the item doesn't have a link, check's the item's active status.
 */
function isActive(location: { path: string }, item: { active: boolean }, exactMatch: boolean = false) {
    if (item.link) {
        return exactMatch
            ? location.path === item.link
            : location.path.startsWith(item.link)
    }
    return item.active
}

const initialState = {
    items: [{
        icon: { default: 'fa fa-home' },
        id: ids.DOCUMENTATION_LINK,
        isActive: location => location.path === routes.HOME
            || location.path.startsWith(routes.INSTRUCTIONS),
        name: 'Documentation',
        link: routes.HOME,
    }, {
        icon: { default: 'fa fa-upload' },
        id: ids.DATA_UPLOAD,
        isActive,
        name: 'Data Upload',
        link: routes.DATA_UPLOAD,
    }, {
        content: (<b style={{ fontSize: 'smaller' }}>Fill</b>),
        icon: { default: 'fa fa-simplybuilt' },
        id: ids.FILL_DEM,
        isActive,
        name: 'Fill DEM',
        link: routes.FILL_DEM,
    }, {
        content: (<b style={{ fontSize: 'smaller' }}>WD</b>),
        icon: { default: 'fa fa-simplybuilt' },
        id: ids.WATERSHED_DELINEATION,
        isActive,
        name: 'Watershed Delineation',
        link: routes.WATERSHED_DELINEATION,
    }, {
        content: (<b style={{ fontSize: 'smaller' }}>SD</b>),
        icon: { default: 'fa fa-simplybuilt' },
        id: ids.SUBCATCHMENT_DELINEATION,
        isActive,
        name: 'Subcatchment Delineation',
        link: routes.SUBCATCHMENT_DELINEATION,
    }, {
        content: (<b style={{ fontSize: 'smaller' }}>Over</b>),
        icon: { default: 'fa fa-simplybuilt' },
        id: ids.DATA_OVERLAY,
        isActive,
        name: 'Data Overlay',
        link: routes.DATA_OVERLAY,
    }, {
        content: (<b style={{ fontSize: 'smaller' }}>HRU</b>),
        icon: { default: 'fa fa-simplybuilt' },
        id: ids.HRU_GENERATION,
        isActive,
        name: 'HRU Generation',
        link: routes.HRU_GENERATION,
    }, {
        content: (<b style={{ fontSize: 'smaller' }}>Topo</b>),
        icon: { default: 'fa fa-simplybuilt' },
        id: ids.TOPOLOGY_GENERATION,
        isActive,
        name: 'Topology Generation',
        link: routes.TOPOLOGY_GENERATION,
    }, {
        content: (<b style={{ fontSize: 'smaller' }}>Input</b>),
        icon: { default: 'fa fa-simplybuilt' },
        id: ids.INPUT_GENERATION,
        isActive,
        name: 'Input File Generation',
        link: routes.INPUT_GENERATION,
    }, {
        icon: { default: 'fa fa-cog' },
        id: ids.SETTINGS,
        isActive,
        name: 'Settings',
        link: routes.SETTINGS,
        position: 'bottom',
    }],
}

export default function contextBar(state = initialState, action) {
    switch (action.type) {
    case CLICK_CONTEXT_BAR_ITEM:
        return {
            ...state,
            items: state.items.map(item => (
                item.id === action.id ?
                    { ...item, active: true } :
                    { ...item, active: false }
            )),
        }

    default:
        return state
    }
}
