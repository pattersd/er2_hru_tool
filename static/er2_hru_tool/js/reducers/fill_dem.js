// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type FillDemType = {
    demResolution: number,
    demResolutionUnit: string,
    detectedDemResolution: number,
    detectedDemResolutionUnit: string,
}

type StateType = {visible: boolean, ...FillDemType}

type ActionType = {
    type: string,
    id: string,
    ...FillDemType,
}

const initialState = {
    visible: true,
    demResolution: 10,
    demResolutionUnit: 'm',
    detectedDemResolution: 0,
    detectedDemResolutionUnit: 'm',
}

export default function fillDem(state: StateType = initialState, action: ActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        // TODO: update this
        if (action.id === ids.FILL_DEM) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state
    case actionTypes.CHANGE_DEM_RESOLUTION:
        return { ...state, demResolution: action.demResolution }
    case actionTypes.CHANGE_DEM_RESOLUTION_UNIT:
        return { ...state, demResolutionUnit: action.demResolutionUnit }
    case actionTypes.CHANGE_DETECTED_DEM_RESOLUTION: {
        const res = action.detectedDemResolution
        const unit = action.detectedDemResolutionUnit
        let selectUnit = unit
        if (!(selectUnit === 'm' || selectUnit === 'km' || selectUnit === 'ft')) {
            selectUnit = state.demResolutionUnit
        }
        return {
            ...state,
            demResolution: res,
            demResolutionUnit: selectUnit,
            detectedDemResolution: res,
            detectedDemResolutionUnit: unit,
        }
    }
    default:
        return state
    }
}
