// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type SettingsType = {
    visible: boolean,
}

export type SettingActionType = {
    active: boolean,
    type: string,
}

const initialState = {
    visible: true,
}

export default function Settings(state: SettingsType = initialState, action: SettingsActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        if (action.id === ids.SETTINGS) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state

    default:
        return state
    }
}
