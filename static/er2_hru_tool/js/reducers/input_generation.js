// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type InputGenerationType = {
    inputModel: string,
}

type StateType = { visible: boolean, ...InputGenerationType }

type ActionType = {
    type: string,
    id: string,
    ...InputGenerationType,
}

const initialState = {
    visible: true,
    inputModel: 'ages',
}

export default function inputGeneration(state: StateType = initialState, action: ActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        // TODO: update this
        if (action.id === ids.FILL_DEM) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state
    case actionTypes.CHANGE_INPUT_MODEL:
        return { ...state, inputModel: action.inputModel }
    default:
        return state
    }
}
