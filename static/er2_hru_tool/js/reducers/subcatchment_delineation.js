// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type SubcatchmentDelineationType = {
    threshold: number,
    thresholdUnit: string,
}

type StateType = { visible: boolean, ...SubcatchmentDelineationType }

type ActionType = {
    type: string,
    id: string,
    ...SubcatchmentDelineationType,
}

const initialState = {
    visible: true,
    threshold: 100,
    thresholdUnit: 'm2',
}

export default function subcatchmentDelineation(state: StateType = initialState, action: ActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        // TODO: update this
        if (action.id === ids.FILL_DEM) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state
    case actionTypes.CHANGE_THRESHOLD:
        return { ...state, threshold: action.threshold }
    case actionTypes.CHANGE_THRESHOLD_UNIT:
        return { ...state, thresholdUnit: action.thresholdUnit }
    default:
        return state
    }
}
