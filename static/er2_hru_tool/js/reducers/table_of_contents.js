// Manages state for the documentation's table of contents
import * as routes from '../constants/routes'

const initialState = {
    items: [{
        id: 'purpose',
        link: routes.INSTRUCTIONS_PURPOSE,
        name: 'Purpose',
    }, {
        id: 'gettingStarted',
        link: routes.INSTRUCTIONS_GETTING_STARTED,
        name: 'Getting Started',
    }, {
        id: 'advanced',
        link: routes.INSTRUCTIONS_ADVANCED,
        name: 'Advanced Users',
    }],
}

export default function tableOfContents(state = initialState, action) {
    switch (action.type) {
    default:
        return state
    }
}