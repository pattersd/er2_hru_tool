// @flow
import * as actionTypes from '../constants/action_types'

export type LogType = {
    log: string[],
    visible: boolean,
}

type ActionType = {
    log?: string[],
    type: string,
}

const initialState = {
    log: [],
    visible: true,
}

export default function dataUpload(state: LogType = initialState, action: ActionType) {
    let ret = state
    if (action.type === actionTypes.SET_LOG) {
        ret = { ...state, log: action.log, visible: true }
    } else if (action.type === actionTypes.APPEND_LOG) {
        ret = { ...state, log: [...state.log, ...action.log], visible: true }
    } else if (action.type === actionTypes.TOGGLE_LOGGER_VISIBILITY) {
        ret = { ...state, visible: !state.visible }
    }

    return ret
}
