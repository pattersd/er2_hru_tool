// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type TopologyGenerationType = {
    dissolveCycles: boolean
}

type StateType = { visible: boolean, ...TopologyGenerationType }

type ActionType = {
    type: string,
    id: string,
    ...TopologyGenerationType,
}

const initialState = {
    visible: true,
    dissolveCycles: false,
}

export default function topologyGeneration(state: StateType = initialState, action: ActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        // TODO: update this
        if (action.id === ids.FILL_DEM) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state
    case actionTypes.CHANGE_DISSOLVE_CYCLES:
        return { ...state, dissolveCycles: action.dissolveCycles }
    default:
        return state
    }
}
