// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type WatershedDelineationType = {
    snapDistance: number,
    snapDistanceUnit: string,
}

type StateType = { visible: boolean, ...WatershedDelineationType }

type ActionType = {
    type: string,
    id: string,
    ...WatershedDelineationType,
}

const initialState = {
    visible: true,
    snapDistance: 100,
    snapDistanceUnit: 'm',
}

export default function watershedDelineation(state: StateType = initialState, action: ActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        // TODO: update this
        if (action.id === ids.WATERSHED_DELINEATION) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state
    case actionTypes.CHANGE_SNAP_DISTANCE:
        return { ...state, snapDistance: action.snapDistance }
    case actionTypes.CHANGE_SNAP_DISTANCE_UNIT:
        return { ...state, snapDistanceUnit: action.snapDistanceUnit }
    default:
        return state
    }
}
