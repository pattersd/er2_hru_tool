// @flow
import * as actionTypes from '../constants/action_types'
import * as ids from '../constants/ids'

export type HruGenerationType = {
    minimumArea: number,
    minimumAreaUnit: string,
}

type StateType = { visible: boolean, ...HruGenerationType }

type ActionType = {
    type: string,
    id: string,
    ...HruGenerationType,
}

const initialState = {
    visible: true,
    minimumArea: 100,
    minimumAreaUnit: 'm2',
}

export default function hruGeneration(state: StateType = initialState, action: ActionType) {
    switch (action.type) {
    case actionTypes.CLICK_CONTEXT_BAR_ITEM:
        // TODO: update this
        if (action.id === ids.FILL_DEM) {
            // Toggle the control bar if clicking an already-active framework button.
            // Otherwise, always show the control bar.
            return (action.active
                ? { ...state, visible: !state.visible }
                : { ...state, visible: true })
        }
        return state
    case actionTypes.CHANGE_MINIMUM_AREA:
        return { ...state, minimumArea: action.minimumArea }
    case actionTypes.CHANGE_MINIMUM_AREA_UNIT:
        return { ...state, minimumAreaUnit: action.minimumAreaUnit }
    default:
        return state
    }
}
