// @flow
import * as types from '../constants/action_types'

export const onClickMapToolbarItem = (id: string) => ({ type: types.CLICK_MAP_TOOLBAR_ITEM, id })
