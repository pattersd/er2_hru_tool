// @flow
import * as types from '../constants/action_types'

export const toggleLoggerVisibility = () => ({ type: types.TOGGLE_LOGGER_VISIBILITY })

export const appendToLogger = (log: string[]) => ({ type: types.APPEND_LOG, log })
