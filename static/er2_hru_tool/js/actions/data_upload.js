// @flow
import request from 'superagent'
import Cookies from 'universal-cookie'
import { setUserLayers } from 'er2-map-userlayers'
import * as types from '../constants/action_types'
import {appendToLogger} from './logger'

export function getCookie(name: string) {
    const cookies = new Cookies()
    return cookies.get(name)
}

type UploadFileType = {
    key: String,
    value: File,
}

export const onDataUpload = (files: UploadFileType[]) => (dispatch: Function, getState: Function) => {
    const req = request.post(`/${getState().meta.name}/uploadProjectFiles/?token=${getState().token.value}`)
        .set('token', getState().token.value)
        .set('X-CSRFToken', getCookie('csrftoken'))
    files.forEach(file => req.attach(file.key, file.value))
    const success = (resp) => {
        console.log(resp)
        if ('resolution' in resp && 'resolutionUnit' in resp) {
            dispatch({
                type: types.CHANGE_DETECTED_DEM_RESOLUTION,
                detectedDemResolution: resp.resolution,
                detectedDemResolutionUnit: resp.resolutionUnit,
            })
            dispatch(setUserLayers(resp.mapsources))
            dispatch(appendToLogger([`Added files ${resp.uploads.join(',')}`]))
        }
    }
    req.then(resp => success(resp.body)).catch(error => alert(error))
}
