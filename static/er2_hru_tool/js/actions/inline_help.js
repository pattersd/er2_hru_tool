import * as types from '../constants/action_types'

// @flow
export const onClickInlineHelp = (id: string) =>
    ({ type: types.CLICK_HELP_INLINE, id })