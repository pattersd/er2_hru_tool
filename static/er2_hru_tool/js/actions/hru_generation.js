// @flow
import * as types from '../constants/action_types'

export const onChangeMinimumArea = (minimumArea: number) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_MINIMUM_AREA, minimumArea })
}

export const onChangeMinimumAreaUnit = (minimumAreaUnit: string) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_MINIMUM_AREA_UNIT, minimumAreaUnit })
}
