import * as types from '../constants/action_types'

// @flow
const setFetchingState = (isFetching: boolean) =>
    ({ type: types.SET_FETCHING_STATE, isFetching })

// @flow
const setFetchedState = (isFetched: boolean, state: { token: { value: string } }, meta: { version: string }) =>
    ({ type: types.SET_FETCHED_STATE, isFetched, state, meta })

// @flow
// Fetches server state, e.g. to initialize the application.
export const fetchState = (location: { params: object }) => (dispatch, getState) => {
    let fetchError = false
    const token = location.params.token ? location.params.token : getState().token.value
    const query = token ? `?token=${token}` : ''
    dispatch(setFetchingState(true))
    fetch(`/${getState().meta.name}/api/v1/state/${query}`).then((response) => {
        fetchError = !response.ok
        return response.json()
    }).then((json) => {
        dispatch(setFetchingState(false))
        if (fetchError) {
            dispatch(setFetchedState(true, {}))
        } else {
            dispatch(setFetchedState(true, json.state, json.meta))
        }
    })
}

export default fetchState
