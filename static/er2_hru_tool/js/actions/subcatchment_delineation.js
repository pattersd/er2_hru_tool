// @flow
import * as types from '../constants/action_types'

export const onChangeThreshold = (threshold: number) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_THRESHOLD, threshold })
}

export const onChangeThresholdUnit = (thresholdUnit: string) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_THRESHOLD_UNIT, thresholdUnit })
}
