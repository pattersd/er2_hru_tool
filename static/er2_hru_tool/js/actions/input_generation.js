// @flow
import * as types from '../constants/action_types'

export const onChangeInputModel = (inputModel: string) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_INPUT_MODEL, inputModel })
}
