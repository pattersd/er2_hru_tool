// @flow
import * as types from '../constants/action_types'

export const onChangeDissolveCycles = (dissolveCycles: boolean) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_DISSOLVE_CYCLES, dissolveCycles })
}
