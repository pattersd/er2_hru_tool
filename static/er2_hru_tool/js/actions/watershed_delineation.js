// @flow
import request from 'superagent'
import * as types from '../constants/action_types'
import { getCookie } from './data_upload'

export const onChangeSnapDistance = (snapDistance: number) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_SNAP_DISTANCE, snapDistance })
}

export const onChangeSnapDistanceUnit = (snapDistanceUnit: string) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_SNAP_DISTANCE_UNIT, snapDistanceUnit })
}

export const onWatershedDelineation = () => (dispatch: Function, getState: Function) => {
    const req = request.post(`/${getState().meta.name}/watershedDelineation/`)
        .set('token', getState().token.value)
        .set('X-CSRFToken', getCookie('csrftoken'))
    const success = resp => console.log(resp)
    const state = getState().watershedDelineation
    req.send({
        snapDistance: state.snapDistance,
        snapDistanceUnit: state.snapDistanceUnit,
    }).then(resp => success(resp.body)).catch(error => alert(error))
}
