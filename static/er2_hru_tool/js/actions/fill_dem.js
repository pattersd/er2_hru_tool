// @flow
import request from 'superagent'
import { appendToLogger } from './logger'
import * as types from '../constants/action_types'
import { getCookie } from './data_upload'

export const onChangeResolution = (demResolution: number) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_DEM_RESOLUTION, demResolution })
}

export const onChangeResolutionUnit = (demResolutionUnit: string) => (dispatch: Function, getState: Function) => {
    dispatch({ type: types.CHANGE_DEM_RESOLUTION_UNIT, demResolutionUnit })
}

export const onFillDem = () => (dispatch: Function, getState: Function) => {
    const req = request.post(`/${getState().meta.name}/fillDem/?token=${getState().token.value}`)
        .set('token', getState().token.value)
        .set('X-CSRFToken', getCookie('csrftoken'))
    const success = resp => {
        dispatch(appendToLogger(resp.log))
    }
    const state = getState().fillDem
    req.send({
        demResolution: state.demResolution,
        demResolutionUnit: state.demResolutionUnit,
    }).then(resp => success(resp.body)).catch(error => alert(error))
}
