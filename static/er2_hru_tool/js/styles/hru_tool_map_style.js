import { StyleSheet } from 'aphrodite'
import { exportAsCss as css } from 'er2_web_utils'

export const styles = StyleSheet.create({
    collapsed: {
        gridRowEnd: 'analysisPanel !important',
    },
    moveLeft: {
        gridColumnStart: 'controlBar',
    },
    moveDown: {
        gridRowEnd: 'bottom',
    },
})


const cssStyles = css(styles)
export default cssStyles
