// Styles for the map container used for selecting a station
import { StyleSheet } from 'aphrodite/no-important'

import { exportAsCss as css } from 'er2_web_utils'

import * as colors from './colors'
import { rawStyles as globals } from './globals'
import { rawStyles as rootStyles } from './root'

export const rawStyles = {
    buttonPrimary: Object.assign({}, rootStyles.buttonPrimary, {
        backgroundColor: colors.primaryOne,
        borderLeft: `1px solid ${colors.white}`,
        color: colors.white,
        fontSize: '20px',
        padding: '5px 10px',
    }),
    collapsed: {
        gridRowEnd: 'analysisPanel',
    },
    container: {
        display: 'flex',
        flexFlow: 'column',
        flexGrow: '1',
        justifyContent: 'flex-start',
        margin: '44px',
        maxWidth: '960px',
        minWidth: '630px',
    },
    content: {
        borderTop: `4px solid ${colors.secondaryOne}`,
        boxShadow: '0 0 10px 1px rgba(0, 0, 0, 0.05)',
        display: 'flex',
        flex: '1 1 auto',
        flexFlow: 'column',
        /* Required to prevent pushing the toolbar below the bottom
           of the screen: https://stackoverflow.com/a/14964944/616937 */
        height: '0px',
        overflowY: 'auto',
    },
    disclaimer: {
        fontStyle: 'italic',
    },
    formTitle: {
        textAlign: 'center',
    },
    left: {
        justifyContent: 'flex-start',
    },
    right: {
        justifyContent: 'flex-end',
    },
    toolbar: {
        backgroundColor: colors.primaryOne,
        boxShadow: '0 -2px 1px -1px rgba(0,0,0,0.2)',
        color: colors.white,
        display: 'flex',
        fontSize: '20px',
        maxWidth: '100%',
    },
}

const subStyles = {
    buttonLeft: Object.assign({}, rawStyles.buttonPrimary, {
        borderLeft: 'none',
        borderRight: `1px solid ${colors.white}`,
    }),
}

export const styles = StyleSheet.create(Object.assign({}, rawStyles, subStyles))

const cssStyles = css(styles)

export default cssStyles