// Theme Colors
// blue
export const highlight = '#588fff'
// dark green
export const primaryOne = '#0b4e29'
export const primaryOneDarken5 = '#08361c'
export const primaryOneLighten5 = '#0e6234'
// gold
export const primaryTwo = '#C8C372'
export const primaryTwoDarken5 = '#b4ae46'
// orange
export const secondaryOne = '#D9782D'
export const secondaryOneDarken5 = '#c66b24'
export const secondaryOneLighten5 = '#dc8441'
// light blue
export const secondaryTwo = '#12A4B6'
// dark gray
export const textColor = '#59595B'

// Standard Colors
export const danger = '#dc3545'
export const dangerDarken5 = '#d42537'
export const dangerLighten5 = '#e04d5c'
export const info = '#898e96'
export const success = '#5cb85c'
export const successDarken5 = '#4cae4c'
export const successLighten5 = '#6ebf6e'
export const warning = '#ffc107'
export const warningDarken5 = '#ebb000'
export const warningLighten5 = '#ffc71f'

// Custom colors
export const gray = '#e3eaed'
export const offwhite = '#f9f9f9'
export const white = '#FFF'
export const blue = '#267fca'
export const blueDarken5 = '#2272b4'
export const blueLighten5 = '#318dd8'

export const closerBackgroundColor = 'rgba(255, 255, 255, 0.6)'
