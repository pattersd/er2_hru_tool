// Global styles, usually merged into other styles objects
import { StyleSheet } from 'aphrodite/no-important'

import { exportAsCss as css } from 'er2_web_utils'

import * as colors from './colors'

export const rawStyles = {
    removeFocus: {
        ':focus': {
            outline: '0',
        },
    },
    button: {
        borderWidth: '0',
        borderRadius: '2px',
        cursor: 'pointer',
    },
    input: {
        ':focus': {
            /* sizes for the 2 images (focus state) */
            backgroundSize: '100% 2px, 100% 1px',
            caretColor: colors.primaryTwo,
            outline: 'none',
        },
        ':invalid': {
            boxShadow: 'none !important',
        },
        ':moz-placeholder': {
            boxShadow: 'none !important',
        },
        background: `${colors.white} no-repeat`,
        /* 2 imgs : 1px gray line (normal state) AND 2px primaryOne line (focus state) */
        backgroundImage: `linear-gradient(to bottom, ${colors.primaryTwo}, ${colors.primaryTwo}),
            linear-gradient(to bottom, ${colors.gray}, ${colors.gray})`,
        /* sizes for the 2 images (default state) */
        backgroundSize: '0 2px, 100% 1px',
        /* positions for the 2 images. Change both "50%" to "0%" or "100%" and tri again */
        backgroundPosition: '50% 100%, 50% 100%',
        border: '0',
        color: colors.textColor,
        padding: '5px',
        /* animation solely on background-size */
        transition: 'background-size 0.3s cubic-bezier(0.64, 0.09, 0.08, 1)',
    },
    label: {
        padding: '5px',
    },
    select: {
        ':focus': {
            /* sizes for the 2 images (focus state) */
            backgroundSize: '100% 2px, 100% 1px',
            caretColor: colors.primaryTwo,
            outline: 'none',
        },
        background: `${colors.white} no-repeat`,
        /* 2 imgs : 1px gray line (normal state) AND 2px primaryOne line (focus state) */
        backgroundImage: `linear-gradient(to bottom, ${colors.primaryTwo}, ${colors.primaryTwo}),
            linear-gradient(to bottom, ${colors.gray}, ${colors.gray})`,
        /* sizes for the 2 images (default state) */
        backgroundSize: '0 2px, 100% 1px',
        /* positions for the 2 images. Change both "50%" to "0%" or "100%" and tri again */
        backgroundPosition: '50% 100%, 50% 100%',
        border: '0',
        color: colors.textColor,
        /* animation solely on background-size */
        transition: 'background-size 0.3s cubic-bezier(0.64, 0.09, 0.08, 1)',
    },
    textArea: {
        ':focus': {
            /* sizes for the 2 images (focus state) */
            backgroundSize: '100% 2px, 100% 1px',
            caretColor: colors.primaryTwo,
            outline: 'none',
        },
        background: `${colors.white} no-repeat`,
        /* 2 imgs : 1px gray line (normal state) AND 2px primaryOne line (focus state) */
        backgroundImage: `linear-gradient(to bottom, ${colors.primaryTwo}, ${colors.primaryTwo}),
            linear-gradient(to bottom, ${colors.gray}, ${colors.gray})`,
        /* sizes for the 2 images (default state) */
        backgroundSize: '0 2px, 100% 1px',
        /* positions for the 2 images. Change both "50%" to "0%" or "100%" and tri again */
        backgroundPosition: '50% 100%, 50% 100%',
        border: '0',
        color: colors.textColor,
        padding: '5px',
        /* animation solely on background-size */
        transition: 'background-size 0.3s cubic-bezier(0.64, 0.09, 0.08, 1)',
    },
    truncatedText: {
        whiteSpace: 'nowrap',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
    },
}

export const styles = StyleSheet.create(rawStyles)

const cssStyles = css(styles)

export default cssStyles