// Styles for the settings panel
import { StyleSheet } from 'aphrodite/no-important'

import { exportAsCss as css } from 'er2_web_utils'

export const styles = StyleSheet.create({

})

const cssStyles = css(styles)

export default cssStyles
