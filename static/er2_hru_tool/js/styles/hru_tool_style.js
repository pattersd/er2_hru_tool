import { StyleSheet } from 'aphrodite'

const style = StyleSheet.create({
    initial: {
        fontWeight: 'initial',
        fontStyle: 'initial',
        color: 'initial',
    },

    isproject: {
        fontWeight: 'bold',
    },

    container: {
        overflow: 'auto',
        marginTop: '10px',
    },
})

export default style
