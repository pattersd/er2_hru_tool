import { createMuiTheme } from 'material-ui/styles/index'

import * as colors from './colors'

export const muiStyles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    formControl: {
        margin: theme.spacing.unit,
    },
})

export const muiTheme = createMuiTheme({
    palette: {
        primary: {
            light: colors.primaryOneLighten5,
            main: colors.primaryOne,
            dark: colors.primaryOneDarken5,
        },
        secondary: {
            light: colors.blueLighten5,
            main: colors.blue,
            dark: colors.blueDarken5,
        },
        error: {
            light: colors.dangerLighten5,
            main: colors.danger,
            dark: colors.dangerDarken5,
        },
    },
    typography: {
        fontFamily: 'Roboto Condensed',
    },
})
