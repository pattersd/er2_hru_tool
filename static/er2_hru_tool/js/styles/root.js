// Styles for the application root

import { StyleSheet } from 'aphrodite/no-important'

import { exportAsCss as css } from 'er2_web_utils'

import * as colors from './colors'
import { rawStyles as globals } from './globals'

export const rawStyles = {
    // Root styles are at the top, all other styles are organized alphabetically
    root: {
        background: '#fff',
        bottom: '0',
        color: 'textColor',
        display: 'grid',
        fontFamily: '\'Roboto Condensed\', sans-serif',
        gridTemplateColumns: `[left] 50px [controlBar] 150px [tocBar] 175px [main] 18px [breadcrumbOverflow]
            3fr [toolbarPanel] 1fr [wideToolbar] 100px [toolbar] 50px [right]`,
        gridTemplateRows: '[top] 52px [subHeader] 44px [main] 2fr [analysisPanel] 1fr [bottom]',
        left: '0',
        overflow: 'hidden',
        margin: '0',
        position: 'absolute',
        right: '0',
        top: '0',
    },
    analysisPanel: {
        boxShadow: '0 -2px 1px -1px rgba(0,0,0,0.2)',
        background: '#fff',
        display: 'flex',
        flexFlow: 'column',
        gridColumnStart: 'main',
        gridColumnEnd: 'right',
        gridRowStart: 'analysisPanel',
        gridRowEnd: 'bottom',
        maxWidth: '100%',
        overflow: 'auto',
        zIndex: 200,
    },
    borderBottomHeader: {
        fontWeight: '500',
        margin: '15px 5px',
        borderBottom: `1px solid ${colors.gray}`,
        paddingBottom: '5px',
    },
    buttonBlue: Object.assign({}, globals.button, globals.removeFocus, {
        ':hover': {
            backgroundColor: colors.blueLighten5,
        },
        ':active': {
            backgroundColor: colors.blueDarken5,
        },
        backgroundColor: colors.blue,
        color: colors.white,
    }),
    buttonPrimary: Object.assign({}, globals.button, globals.removeFocus, {
        ':hover': {
            backgroundColor: colors.primaryOneLighten5,
        },
        ':active': {
            backgroundColor: colors.primaryOneDarken5,
        },
        backgroundColor: colors.primaryOne,
        color: colors.white,
    }),
    buttonSecondary: Object.assign({}, globals.button, globals.removeFocus, {
        ':hover': {
            backgroundColor: colors.secondaryOneLighten5,
        },
        ':active': {
            backgroundColor: colors.secondaryOneDarken5,
        },
        backgroundColor: colors.secondaryOne,
        color: colors.white,
    }),
    buttonSuccess: Object.assign({}, globals.button, globals.removeFocus, {
        ':active': {
            backgroundColor: colors.successDarken5,
        },
        ':hover': {
            backgroundColor: colors.successLighten5,
        },
        backgroundColor: colors.success,
        color: colors.white,
    }),
    card: {
        padding: '10px',
    },
    cardBody: {
        maxHeight: '500px',
        overflowY: 'auto',
    },
    closerHorizontal: {
        backgroundColor: colors.closerBackgroundColor,
        gridColumnStart: 'main',
        gridColumnEnd: 'toolbar',
        gridRowStart: 'analysisPanel',
        gridRowEnd: 'bottom',
        position: 'absolute',
        top: '-20px',
        left: '10px',
        width: '30px',
        textAlign: 'center',
        zIndex: 200,
    },
    closerVertical: {
        backgroundColor: colors.closerBackgroundColor,
        gridColumnStart: 'main',
        gridColumnEnd: 'main',
        gridRowStart: 'subHeader',
        gridRowEnd: 'bottom',
        height: '30px',
        position: 'absolute',
        top: '0px',
        left: '10px',
        textAlign: 'center',
        verticalAlign: 'middle',
        width: '20px',
        zIndex: 200,
    },
    controlBar: {
        boxShadow: '2px 0 1px -1px rgba(0,0,0,0.2)',
        composes: 'aboveMap',
        display: 'flex',
        flexDirection: 'column',
        gridColumnEnd: 'main',
        gridColumnStart: 'controlBar',
        gridRowEnd: 'bottom',
        gridRowStart: 'main',
        overflow: 'auto',
    },
    controlBarBottom: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: '1',
    },
    controlBarTop: {
        display: 'flex',
        flexDirection: 'column',
        flexGrow: '1',
    },
    error: {
        color: colors.danger,
    },
    errorInlineCtr: {
        border: `1px solid ${colors.danger}`,
        borderRadius: '2px',
        margin: '10px',
        padding: '10px',
    },
    flexTop: {
        flexGrow: '1',
    },
    form: {
        alignItems: 'stretch',
        display: 'flex',
        flexFlow: 'column',
        gridColumnStart: 'controlBar',
        gridColumnEnd: 'right',
        gridRowStart: 'subHeader',
        gridRowEnd: 'bottom',
    },
    formGroup: {
        display: 'flex',
        justifyContent: 'space-between',
        padding: '10px',
    },
    formItem: {
        padding: '5px',
    },
    formTop: {
        flewGrow: '1',
    },
    header: {
        fontFamily: '\'Roboto Condensed\', sans-serif',
    },
    headerLink: {
        color: colors.white,
        textDecoration: 'none',
    },
    helpIcon: Object.assign({}, globals.removeFocus, {
        ':active': {
            color: colors.warningDarken5,
        },
        ':hover': {
            color: colors.warningLighten5,
        },
        paddingLeft: '10px',
        color: colors.gray,
        cursor: 'pointer',
    }),
    helpIconActive: {
        color: `${colors.warning} !important`,
    },
    helpInlineCtr: {
        border: `1px solid ${colors.warning}`,
        borderRadius: '2px',
        margin: '10px',
        padding: '10px',
    },
    hidden: {
        display: 'none',
    },
    highchartsCtr: {
        width: '100%',
    },
    input: Object.assign({}, globals.input, {}),
    instructions: {
        gridColumnEnd: 'right',
        gridColumnStart: 'main',
        gridRowEnd: 'bottom',
        gridRowStart: 'main',
        overflow: 'auto',
        padding: '10px',
    },
    label: Object.assign({}, globals.label, {}),
    link: {
        ':visited': {
            color: colors.primaryOne,
            textDecoration: 'none',
        },
        ':hover': {
            color: colors.primaryTwo,
            textDecoration: 'underline',
        },
        color: colors.primaryOne,
        textDecoration: 'none',
    },
    loading: {
        alignItems: 'center',
        display: 'flex',
        flexFlow: 'column',
        gridColumnEnd: 'right',
        gridColumnStart: 'controlBar',
        gridRowEnd: 'bottom',
        gridRowStart: 'subHeader',
        justifyContent: 'center',
    },
    logoCtr: {
        display: 'inline-block',
        fontSize: '20px',
        margin: '10px',
        textDecoration: 'none',
        textTransform: 'uppercase',
    },
    logoVersion: {
        fontSize: '12px',
        padding: '0 5px',
    },
    moveLeft: {
        gridColumnStart: 'controlBar',
    },
    moveDown: {
        gridRowEnd: 'bottom',
    },
    moveUp: {
        gridRowStart: 'top',
    },
    noResults: {
        textAlign: 'center',
    },
    row: {
        display: 'flex',
        alignItems: 'center',
        padding: '5px 0',
    },
    map: {
        height: '100%',
    },
    mapCtr: {
        gridColumnStart: 'main',
        gridColumnEnd: 'toolbar',
        gridRowStart: 'subHeader',
        gridRowEnd: 'analysisPanel',
    },
    mapToolbarTitleBar: {
        backgroundColor: colors.secondaryTwo,
        color: colors.white,
        flex: '0 1',
        maxWidth: '100%',
        lineHeight: '44px',
        margin: '0',
        overflow: 'inherit',
        textAlign: 'center',
    },
    scroll: {
        /* Prevent flex containers from squishing */
        overflowY: 'auto',
    },
    select: Object.assign({}, globals.select, {}),
    startDown: {
        gridRowStart: 'bottom',
    },
    startLeft: {
        gridColumnStart: 'controlBar',
    },
    startUp: {
        gridRowStart: 'top',
    },
    table: {
        display: 'flex',
        flexFlow: 'column nowrap',
        flexGrow: '1',
        flexShrink: '1',
    },
    tableOfContentsBar: {
        backgroundColor: 'white',
        gridColumnEnd: 'main',
        gridColumnStart: 'controlBar',
        gridRowEnd: 'bottom',
        gridRowStart: 'main',
    },
    tableOfContentsBarItem: {
        ':active': {
            color: colors.primaryTwoDarken5,
        },
        ':hover': {
            color: colors.primaryTwoDarken5,
        },
        borderBottom: `1px solid ${colors.gray}`,
        cursor: 'pointer',
        color: 'primaryOne',
        display: 'block',
        fontSize: '16px',
        padding: '10px',
        textAlign: 'right',
        textDecoration: 'none',
    },
    textArea: Object.assign({}, globals.textArea, {}),
    titleBar: Object.assign({}, globals.truncatedText, {
        color: colors.white,
        backgroundColor: colors.primaryOne,
        flex: '1',
        fontSize: '20px',
        height: '44px',
        lineHeight: '44px',
        margin: '0',
        maxWidth: 'calc(100% - 50px)',
        overflow: 'hidden',
        paddingLeft: '10px',
        textAlign: 'left',
    }),
    td: {
        display: 'flex',
        flexBasis: '0',
        flexFlow: 'row nowrap',
        flexGrow: '4',
        padding: '0.5em',
        textOverflow: 'ellipsis',
        wordBreak: 'break-all',
    },
    tr: {
        display: 'flex',
        flexFlow: 'row nowrap',
    },
}

export const styles = StyleSheet.create(rawStyles)

const cssStyles = css(styles)

export default cssStyles
