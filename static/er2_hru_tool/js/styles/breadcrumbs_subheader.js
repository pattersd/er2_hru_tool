// These styles are merged with styles from er2_ui to properly display
// the subheader for the select station map step of the application.
import { StyleSheet } from 'aphrodite/no-important'

import { exportAsCss as css } from 'er2_web_utils'

export const styles = StyleSheet.create({
    breadcrumbHeader: {
        width: '100%',
    },
    breadcrumbs: {
        gridColumnEnd: 'breadcrumbOverflow',
    },
    headerTextContent: {
        flexGrow: '1',
    },
})

const cssStyles = css(styles)

export default cssStyles