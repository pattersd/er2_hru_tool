// These styles are merged with styles from er2_ui to properly display
// the items in the table of contents.
import { StyleSheet } from 'aphrodite/no-important'

import { exportAsCss as css } from 'er2_web_utils'

import * as colors from './colors'

export const styles = StyleSheet.create({
    active: {
        ':active': {
            color: colors.primaryTwoDarken5,
        },
        ':hover': {
            color: colors.primaryTwoDarken5,
        },
        color: colors.primaryTwo,
    },
})

const cssStyles = css(styles)

export default cssStyles