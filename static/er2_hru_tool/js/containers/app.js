// @flow
import React from 'react'
import { css } from 'aphrodite'
import { connect } from 'react-redux'
import { NavLink, Route, withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { parse, stringify } from 'query-string'
import { MuiThemeProvider } from 'material-ui/styles'
import MapBaseLayers from 'er2-map-baselayers'
import Mapsources from 'er2-map-userlayers/js/components/mapsources'

import { Breadcrumbs, ContextBar, Header, MapToolbar, MapToolbarPanel, styles as uiStyles, TokenNavLink } from 'er2_ui'
import { mergeCss, parseLocation } from 'er2_web_utils'

import * as Actions from '../actions'
import Closer from '../components/closer'
import Logger from '../components/logger'
import { muiTheme } from '../styles/mui'
import DataUpload from '../components/data_upload'
import FillDem from '../components/fill_dem'
import WatershedDelineation from '../components/watershed_delineation'
import SubcatchmentDelineation from '../components/subcatchment_delineation'
import DataOverlay from '../components/data_overlay'
import HruGeneration from '../components/hru_generation'
import TopologyGeneration from '../components/topology_generation'
import InputGeneration from '../components/input_generation'
import HruToolMap from '../components/hru_tool_map'
import Instructions from '../components/instructions'
import Settings from '../components/settings'
import TableOfContents from '../components/table_of_contents'
import * as ids from '../constants/ids'
import * as routes from '../constants/routes'
import { styles as subheaderStyles } from '../styles/breadcrumbs_subheader'
import rootCss, { styles as rootCssStyles } from '../styles/root'
import { styles as tableOfContentsBarItemStyles } from '../styles/table_of_contents_bar_item'
import type { LogType } from '../reducers/logger'
import type { MapType } from '../reducers/map'
import type { MapToolbarItemType } from '../reducers/map_toolbar'
import type { DataUploadType } from '../reducers/data_upload'
import type { FillDemType } from '../reducers/fill_dem'
import type { WatershedDelineationType } from '../reducers/watershed_delineation'
import type { SubcatchmentDelineationType } from '../reducers/subcatchment_delineation'
import type { DataOverlayType } from '../reducers/data_overlay'
import type { HruGenerationType } from '../reducers/hru_generation'
import type { TopologyGenerationType } from '../reducers/topology_generation'
import type { InputGenerationType } from '../reducers/input_generation'
import type { SettingsType } from '../reducers/settings'

type Props = {
    actions: {
        fetchState: Function,
        onClickFormBreadcrumb: Function,
        onClickContextBarItem: Function,
        onClickInlineHelp: Function,
        onChangeBaseLayer: Function,
        onChangeLayerProps: Function,
        onChangeResolution: Function,
        onChangeResolutionUnit: Function,
        onDataUpload: Function,
        onFillDem: Function,
        onChangeSnapDistance: Function,
        onChangeSnapDistanceUnit: Function,
        onWatershedDelineation: Function,
        onChangeThreshold: Function,
        onChangeThresholdUnit: Function,
        onChangeOverlays: Function,
        onChangeMinimumArea: Function,
        onChangeMinimumAreaUnit: Function,
        onChangeDissolveCycles: Function,
        onChangeInputModel: Function,
        onUserLayersAdd: Function,
        onUserLayerDelete: Function,
        onUserLayersDelete: Function,
        onUserLayersPropsChange: Function,
        onUserLayersSort: Function,
        onUserLayersZoom: Function,
        toggleLoggerVisibility: Function,
    },
    dataUpload: DataUploadType,
    fillDem: FillDemType,
    watershedDelineation: WatershedDelineationType,
    subcatchmentDelineation: SubcatchmentDelineationType,
    dataOverlay: DataOverlayType,
    hruGeneration: HruGenerationType,
    topologyGeneration: TopologyGenerationType,
    inputGeneration: InputGenerationType,
    contextBar: {
        items: [{
            id: string,
            link?: string,
            name: string,
        }],
    },
    form: {
        startDate: string,
    },
    history: {
        action: string,
        block: Function,
        length: number,
        push: Function,
        replace: Function,
        go: Function,
        goBack: Function,
        goForward: Function,
    },
    location: {
        pathname: string,
        search?: string,
        hash?: string,
    },
    logger: LogType,
    map: MapType,
    mapToolbar: {
        items: [{
            icon: {
                default: string,
                active?: string,
            },
            id: string,
            link?: string,
            name: string,
        }],
        panel: {
            visible: boolean,
        },
    },
    match: {
        params: {},
        isExact: boolean,
        path?: string,
        url?: string,
    },
    meta: {
        name: string,
        version: string,
    },
    serverState: {
        isFetched: boolean,
        isFetching: boolean,
    },
    settings: SettingsType,
    tableOfContents: {
        items: [{
            id: string,
            link: string,
            name: string,
        }],
    },
    token: {
        value: string,
        expires: number,
    },
}

/* eslint-disable react/prefer-stateless-function */
class App extends React.Component<Props> {
    componentDidMount() {
        // Fetch the application state once, on initial load
        const parsedLoc = parseLocation(location)
        if (!this.props.serverState.isFetched && !this.props.serverState.isFetching) {
            this.props.actions.fetchState(parsedLoc)
        }
    }

    componentWillReceiveProps(nextProps) {
        // Append the token to the URL to sync state, when available
        if (nextProps.token.value) {
            const params = parse(this.props.location.search)
            if (!params.token || params.token !== nextProps.token.value) {
                params.token = nextProps.token.value
                this.props.history.push({
                    search: stringify(params),
                })
            }
        }
    }

    render() {
        const parsedLoc = parseLocation(location)

        if (!this.props.token.value) {
            // Render loading page until the server state is available
            return (<div className={rootCss.root}>
                <Header meta={this.props.meta}>
                    <div className={rootCss.logoCtr}>
                        {this.props.token.value
                            ? (<TokenNavLink
                                className={rootCss.headerLink}
                                token={this.props.token}
                                role="button"
                                to={routes.HOME}
                            >
                                <span className={rootCss.logo}>HRU Delineation Tool</span>
                                <span className={rootCss.logoVersion}>v. {this.props.meta.version}</span>
                            </TokenNavLink>)
                            : (<NavLink
                                className={rootCss.headerLink}
                                token={this.props.token}
                                role="button"
                                to={routes.HOME}
                            >
                                <span className={rootCss.logo}>HRU Delineation Tool</span>
                                <span className={rootCss.logoVersion}>v. {this.props.meta.version}</span>
                            </NavLink>)
                        }
                    </div>
                </Header>
                <ContextBar
                    items={this.props.contextBar.items}
                    state={this.props}
                    token={this.props.token}
                    onClickContextBarItem={this.props.actions.onClickContextBarItem}
                />
                <div className={rootCss.loading}>
                    <i className="fa fa-cog fa-spin fa-3x fa-fw" />
                    <span className="sr-only">Loading...</span>
                </div>
            </div>)
        }

        const tocVisible =
            (location.pathname === routes.SETTINGS && this.props.settings.visible) ||
            (location.pathname === routes.DATA_UPLOAD && this.props.dataUpload.visible) ||
            (location.pathname === routes.FILL_DEM && this.props.fillDem.visible) ||
            (location.pathname === routes.WATERSHED_DELINEATION && this.props.watershedDelineation.visible) ||
            (location.pathname === routes.SUBCATCHMENT_DELINEATION && this.props.subcatchmentDelineation.visible) ||
            (location.pathname === routes.DATA_OVERLAY && this.props.dataOverlay.visible) ||
            (location.pathname === routes.HRU_GENERATION && this.props.hruGeneration.visible) ||
            (location.pathname === routes.TOPOLOGY_GENERATION && this.props.topologyGeneration.visible) ||
            (location.pathname === routes.INPUT_GENERATION && this.props.inputGeneration.visible)

        const maptoolbarVisible = location.pathname === routes.SETTINGS ||
            location.pathname === routes.DATA_OVERLAY ||
            location.pathname === routes.DATA_UPLOAD ||
            location.pathname === routes.FILL_DEM ||
            location.pathname === routes.WATERSHED_DELINEATION ||
            location.pathname === routes.SUBCATCHMENT_DELINEATION ||
            location.pathname === routes.HRU_GENERATION ||
            location.pathname === routes.TOPOLOGY_GENERATION ||
            location.pathname === routes.INPUT_GENERATION

        const panels = this.props.mapToolbar.items.filter((item: MapToolbarItemType) => item.active).map(item => (
            <MapToolbarPanel
                key={item.id}
                mapToolbar={this.props.mapToolbar}
                {...this.props.actions}
            >
                {item.id === ids.SELECT_BASE_LAYERS && (
                    <div style={{ height: '100%' }}>
                        <h3 className={css(rootCssStyles.mapToolbarTitleBar)}>Map Settings</h3>

                        <div style={{ margin: 'auto', width: '260px' }}>
                            <div style={{ textAlign: 'left', marginTop: '10px', marginBottom: '30px', marginLeft: '20px' }}>
                                <span style={{ color: '#888', fontFamily: 'Roboto Condensed', fontWeight: 'bold' }}>Base Layers</span>
                            </div>

                            <MapBaseLayers
                                baseImageURL="/static/er2_hru_tool/dist"
                                baseLayer={this.props.map.baseLayer}
                                onSetBaseLayer={this.props.actions.onChangeBaseLayer}
                            />
                        </div>
                    </div>
                )}
                {item.id === ids.USER_LAYERS && (
                    <div style={{ height: '100%' }}>
                        <h3 className={css(rootCssStyles.mapToolbarTitleBar)}>User Layers</h3>
                        <Mapsources
                            mapsources={this.props.map.mapsources}
                            onAddLayers={this.props.actions.onUserLayersAdd}
                            onSortLayers={this.props.actions.onUserLayersSort}
                            onChangeLayerProps={this.props.actions.onUserLayersPropsChange}
                            onDeleteLayer={this.props.actions.onUserLayerDelete}
                            onZoomLayers={this.props.actions.onUserLayersZoom}
                            {...this.props.actions}
                        />
                    </div>
                )}
            </MapToolbarPanel>
        ))

        const sub = s => s.substr(1, s.length - 2)
        const mapPath = `/(${sub(routes.DATA_UPLOAD)}|${sub(routes.FILL_DEM)}|${sub(routes.WATERSHED_DELINEATION)}|${sub(routes.SUBCATCHMENT_DELINEATION)}|${sub(routes.DATA_OVERLAY)}|${sub(routes.HRU_GENERATION)}|${sub(routes.TOPOLOGY_GENERATION)}|${sub(routes.INPUT_GENERATION)}|${sub(routes.SETTINGS)})/`
        const hasLogger = this.props.logger.log.length > 0

        return (
            <MuiThemeProvider theme={muiTheme}>
                <div className={rootCss.root}>
                    <Header token={this.props.token} meta={this.props.meta}>
                        <div className={rootCss.logoCtr}>
                            {this.props.token.value
                                ? (<TokenNavLink
                                    className={rootCss.headerLink}
                                    token={this.props.token}
                                    role="button"
                                    to={routes.HOME}
                                >
                                    <span className={rootCss.logo}>HRU Delineation Tool</span>
                                    <span className={rootCss.logoVersion}>v. {this.props.meta.version}</span>
                                </TokenNavLink>)
                                : (<NavLink
                                    className={rootCss.headerLink}
                                    token={this.props.token}
                                    role="button"
                                    to={routes.HOME}
                                >
                                    <span className={rootCss.logo}>HRU Delineation Tool</span>
                                    <span className={rootCss.logoVersion}>v. {this.props.meta.version}</span>
                                </NavLink>)
                            }
                        </div>
                    </Header>
                    <ContextBar
                        items={this.props.contextBar.items}
                        state={this.props}
                        token={this.props.token}
                        onClickContextBarItem={this.props.actions.onClickContextBarItem}
                    />
                    <Route
                        path={routes.HOME}
                        exact
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.SUBHEADER_DOCS,
                                    name: 'HRU Delineation Tool',
                                }}
                                items={[]}
                            />)}
                    />
                    <Route
                        path={routes.INSTRUCTIONS}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.SUBHEADER_DOCS,
                                    name: 'HRU Delineation Tool',
                                }}
                                items={[]}
                            />)}
                    />

                    {tocVisible &&
                    <Route
                        path={routes.SETTINGS}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.SUBHEADER_SETTINGS,
                                    name: 'Settings',
                                }}
                                items={[]}
                            />)}
                    />
                    }
                    <Route
                        path={routes.HOME}
                        exact
                        render={() =>
                            <Instructions theme={rootCss} token={this.props.token} />}
                    />
                    <Route
                        path={routes.HOME}
                        exact
                        render={() =>
                            (<TableOfContents
                                items={this.props.tableOfContents.items}
                                theme={mergeCss(rootCssStyles, tableOfContentsBarItemStyles)}
                                token={this.props.token}
                            />)}
                    />
                    <Route
                        path={routes.INSTRUCTIONS}
                        render={() => <Instructions theme={rootCss} token={this.props.token} />}
                    />
                    <Route
                        path={routes.INSTRUCTIONS}
                        render={() =>
                            (<TableOfContents
                                items={this.props.tableOfContents.items}
                                theme={mergeCss(rootCssStyles, tableOfContentsBarItemStyles)}
                                token={this.props.token}
                            />)}
                    />
                    {this.props.settings.visible && (
                        <Route
                            path={routes.SETTINGS}
                            render={() => (
                                <Settings
                                    theme={rootCssStyles}
                                    {...this.props.actions}
                                />)
                            }
                        />
                    )}

                    {hasLogger && (
                        <Closer
                            isOpen={this.props.logger.visible}
                            moveLeft={!tocVisible}
                            onToggle={this.props.actions.toggleLoggerVisibility}
                            type="south"
                        />
                    )}
                    {hasLogger && this.props.logger.visible && (
                        <Route
                            path={mapPath}
                            render={() => (
                                <Logger
                                    log={this.props.logger.log}
                                    theme={rootCss}
                                    tocVisible={tocVisible}
                                    {...this.props.actions}
                                />)
                            }
                        />
                    )}

                    {this.props.mapToolbar.panel.visible && maptoolbarVisible &&
                    <Route
                        path={`${routes.HOME}(.+)`}
                        render={() => panels}
                    />
                    }

                    {maptoolbarVisible && (
                        <Route
                            path={`${routes.HOME}(.+)`}
                            render={() => (
                                <MapToolbar
                                    mapToolbar={this.props.mapToolbar}
                                    {...this.props.actions}
                                />)
                            }
                        />
                    )}

                    {tocVisible &&
                    <Route
                        path={routes.DATA_UPLOAD}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.DATA_UPLOAD,
                                    name: 'Data Upload',
                                }}
                                items={[]}
                            />)}
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.DATA_UPLOAD}
                        render={() => (
                            <DataUpload
                                theme={rootCssStyles}
                                {...this.props.actions}
                            />)
                        }
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.FILL_DEM}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.FILL_DEM,
                                    name: 'Fill DEM',
                                }}
                                items={[]}
                            />)}
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.FILL_DEM}
                        render={() => (
                            <FillDem
                                theme={rootCssStyles}
                                demResolution={this.props.fillDem.demResolution}
                                demResolutionUnit={this.props.fillDem.demResolutionUnit}
                                detectedDemResolution={this.props.fillDem.detectedDemResolution}
                                detectedDemResolutionUnit={this.props.fillDem.detectedDemResolutionUnit}
                                onChangeResolution={this.props.actions.onChangeResolution}
                                onChangeResolutionUnit={this.props.actions.onChangeResolutionUnit}
                                onFillDem={this.props.actions.onFillDem}
                            />)
                        }
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.WATERSHED_DELINEATION}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.WATERSHED_DELINEATION,
                                    name: 'Watershed Delineation',
                                }}
                                items={[]}
                            />)}
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.WATERSHED_DELINEATION}
                        render={() => (
                            <WatershedDelineation
                                theme={rootCssStyles}
                                snapDistance={this.props.watershedDelineation.snapDistance}
                                snapDistanceUnit={this.props.watershedDelineation.snapDistanceUnit}
                                onChangeSnapDistance={this.props.actions.onChangeSnapDistance}
                                onChangeSnapDistanceUnit={this.props.actions.onChangeSnapDistanceUnit}
                                onWatershedDelineation={this.props.actions.onWatershedDelineation}
                            />)
                        }
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.SUBCATCHMENT_DELINEATION}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.SUBCATCHMENT_DELINEATION,
                                    name: 'Subcatchment Delineation',
                                }}
                                items={[]}
                            />)}
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.SUBCATCHMENT_DELINEATION}
                        render={() => (
                            <SubcatchmentDelineation
                                theme={rootCssStyles}
                                threshold={this.props.subcatchmentDelineation.threshold}
                                thresholdUnit={this.props.subcatchmentDelineation.thresholdUnit}
                                onChangeThreshold={this.props.actions.onChangeThreshold}
                                onChangeThresholdUnit={this.props.actions.onChangeThresholdUnit}
                            />)
                        }
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.DATA_OVERLAY}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.DATA_OVERLAY,
                                    name: 'Data Overlay',
                                }}
                                items={[]}
                            />)}
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.DATA_OVERLAY}
                        render={() => (
                            <DataOverlay
                                theme={rootCssStyles}
                                overlays={this.props.dataOverlay.overlays}
                                onChangeOverlays={this.props.actions.onChangeOverlays}
                            />)
                        }
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.HRU_GENERATION}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.HRU_GENERATION,
                                    name: 'HRU Generation',
                                }}
                                items={[]}
                            />)}
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.HRU_GENERATION}
                        render={() => (
                            <HruGeneration
                                theme={rootCssStyles}
                                minimumArea={this.props.hruGeneration.minimumArea}
                                minimumAreaUnit={this.props.hruGeneration.minimumAreaUnit}
                                onChangeMinimumArea={this.props.actions.onChangeMinimumArea}
                                onChangeMinimumAreaUnit={this.props.actions.onChangeMinimumAreaUnit}
                            />)
                        }
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.TOPOLOGY_GENERATION}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.TOPOLOGY_GENERATION,
                                    name: 'Topology Generation',
                                }}
                                items={[]}
                            />)}
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.TOPOLOGY_GENERATION}
                        render={() => (
                            <TopologyGeneration
                                theme={rootCssStyles}
                                dissolveCycles={this.props.topologyGeneration.dissolveCycles}
                                onChangeDissolveCycles={this.props.actions.onChangeDissolveCycles}
                            />)
                        }
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.INPUT_GENERATION}
                        render={() => (
                            <Breadcrumbs
                                theme={mergeCss(uiStyles.breadcrumbs.styles, subheaderStyles)}
                                header={{
                                    id: ids.INPUT_GENERATION,
                                    name: 'Input File Generation',
                                }}
                                items={[]}
                            />)}
                    />
                    }

                    {tocVisible &&
                    <Route
                        path={routes.INPUT_GENERATION}
                        render={() => (
                            <InputGeneration
                                theme={rootCssStyles}
                                inputModel={this.props.inputGeneration.inputModel}
                                onChangeInputModel={this.props.actions.onChangeInputModel}
                            />)
                        }
                    />
                    }

                    <Route
                        path={mapPath}
                        render={() => (
                            <HruToolMap
                                baseLayer={this.props.map.baseLayer}
                                extent={this.props.map.extent}
                                mapsources={this.props.map.mapsources}
                                moveLeft={!tocVisible}
                                moveDown
                                theme={rootCssStyles}
                                {...this.props.actions}
                            />)
                        }
                    />
                </div>
            </MuiThemeProvider>
        )
    }
}

App.defaultProps = {
    match: { params: {} },
    mapToolbar: { items: [] },
    tableOfContents: { items: [] },
}

const mapStateToProps = state => ({
    dataUpload: state.dataUpload,
    fillDem: state.fillDem,
    watershedDelineation: state.watershedDelineation,
    subcatchmentDelineation: state.subcatchmentDelineation,
    dataOverlay: state.dataOverlay,
    hruGeneration: state.hruGeneration,
    topologyGeneration: state.topologyGeneration,
    inputGeneration: state.inputGeneration,
    logger: state.logger,
    contextBar: state.contextBar,
    form: state.form,
    map: state.map,
    mapToolbar: state.mapToolbar,
    meta: state.meta,
    selectedStation: state.selectedStation,
    selectStationBar: state.selectStationBar,
    selectStationMap: state.selectStationMap,
    serverState: state.serverState,
    settings: state.settings,
    tableOfContents: state.tableOfContents,
    token: state.token,
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(Actions, dispatch),
})

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps,
)(App))
