/* eslint-disable */
const common = require('./webpack.common.js')
const merge = require('webpack-merge')
const webpack = require('webpack')

module.exports = merge(common, {
    devtool: 'hidden-source-map',
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new webpack.optimize.UglifyJsPlugin({ mangle: false, sourcemap: false }),
    ],
})