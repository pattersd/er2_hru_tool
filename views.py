import json
import logging
import os
import zipfile

import django.shortcuts
from django.views.decorators.csrf import csrf_exempt

import er2.common.api as er2_api
import er2.apps.er2_map.gis as er2_gis
from er2.apps.er2_map.mapsource.mapsource import create_mapsource, mapsource_factory
from er2.apps.er2_map.gis.layer.layer import layer_factory
from er2.apps.er2_map.gis.layer.raster import Raster
from er2.apps.er2_hru_tool import accumulation as ac
from er2.apps.er2_hru_tool import fill_dem as fd
from er2.apps.er2_hru_tool import services as svcs
from er2.apps.er2_hru_tool import watershed_delineation as wd
from er2.apps.er2_map import services as svcs_map

logger = logging.getLogger(__name__)


@csrf_exempt
def index(request):
    ctx = {"version": os.getenv("APP_VERSION", "unknown")}
    return django.shortcuts.render_to_response("er2_hru_tool/index.html", context=ctx)


@csrf_exempt
def state(request):
    """Returns the state of an application."""
    return svcs.State(request=request).process()


@csrf_exempt
def is_zip_file(content_type):
    return content_type == 'application/zip' or \
           content_type == 'application/x-zip-compressed' or \
           content_type == 'application/zip-compressed'


@csrf_exempt
def upload_project_files(request):
    """Upload zip file with AgES project"""
    uploaded_files = []
    dir = '/files/'

    # Create or update mapsource
    map_svc = svcs_map.State(request=request)
    mapsource_dir = map_svc.get_mapsource_dir()
    mapsource = mapsource_factory(os.path.join(mapsource_dir, 'files.mapsource'))
    if not mapsource:
        mapsource = create_mapsource('mapserver', mapsource_dir=mapsource_dir)

    if request.method == "POST":
        for name, upload in request.FILES.items():
            logger.info('Processing: ' + name)
            if name == 'outlet':
                gis_src = None
                if is_zip_file(upload.content_type):
                    zipf = zipfile.ZipFile(upload)
                    zipf.extractall(path=dir)
                    for fname in zipf.namelist():
                        ext = fname[fname.index('.'):]
                        nf = name + ext
                        os.rename(dir + fname, dir + nf)
                        if ext in er2_gis.layer.layer.vector_exts or ext in er2_gis.layer.layer.raster_exts:
                            gis_src = dir + nf
                    uploaded_files.append(name)

                    if gis_src:
                        l = layer_factory(gis_src)
                        l.classify()
                        l.set_properties({'src': name})
                        l.save_meta()
            else:
                if 'image/tiff' == upload.content_type:
                    rasterpath = dir + name + '.tif'
                    fout = open(rasterpath, 'wb+')
                    for chunk in upload.chunks():
                        fout.write(chunk)
                    fout.close()
                    uploaded_files.append(name)
                    l = layer_factory(rasterpath)
                    l.classify('Equal Distance', 'pixel', '#FF0000', '#0000FF', 5)
                    l.set_properties({'src': name})
                    l.save_meta()

    mapsource.update_map_layers()

    response = {"uploads": uploaded_files}
    if "dem" in uploaded_files:
        response.update(fd.get_dem_resolution())

    newstate = svcs.State(request=request).get_state()
    response.update({'mapsources': newstate['mapsources']})
    return er2_api.respond(response)


@csrf_exempt
def fill_dem(request):
    log_file_path = '/tmp/fill_dem.log'
    if os.path.exists(log_file_path):
        os.unlink(log_file_path)

    map_svc = svcs_map.State(request=request)
    mapsource_dir = map_svc.get_mapsource_dir()
    mapsource = mapsource_factory(os.path.join(mapsource_dir, 'files.mapsource'))

    if 'POST' == request.method:
        # output to file
        fh = logging.FileHandler(log_file_path)
        logger.addHandler(fh)

        if 'application/json' == request.content_type:
            body = json.loads(request.body.decode('utf-8'))
            resolution = int(body['demResolution'])
            unit = str(body['demResolutionUnit'])
            logger.info('Dem Resolution: %s%s' % (resolution, unit))
            fd.fill_dem(resolution, unit, logger)
            ac.accumulation(logger)

            for raster in ['dem_filled.tif']:
                l = layer_factory(os.path.join(mapsource.path, raster))
                l.classify('Equal Distance', 'pixel', '#FF0000', '#0000FF', 5)
                l.set_properties({'src': 'fill_dem'})
                l.save_meta()

    # Update files mapsource
    mapsource.update_map_layers()

    with open(log_file_path) as fp:
        log = fp.readlines()
        return er2_api.respond({'log': log, 'result: ': []})


@csrf_exempt
def watershed_delineation(request):
    if 'POST' == request.method:
        if 'application/json' == request.content_type:
            body = json.loads(request.body.decode('utf-8'))
            snap = int(body['snapDistance'])
            unit = str(body['snapDistanceUnit'])
            logger.info('Snap Distance: %s%s' % (snap, unit))
            wd.watershed_delineation(snap, unit)

    return er2_api.respond({'result: ': []})
