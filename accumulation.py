# -*- coding: utf-8 -*-
'''

 MODULE:       eRAMS2.0_HRU_delineation

 AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
               IRSTEA - France
               by Holm Kipka CSU

 PURPOSE:      Prepare files for the next steps of HRU Delineation
                   shapefile : selected gauges
                   rasters :   bounded DEM

'''
import logging
import string, subprocess, os, shutil, sys, glob, types
import math
import configparser
import grass.script as grass

from osgeo import gdal
from osgeo import ogr

from er2.apps.er2_hru_tool import hru_tool_settings as settings


def accumulation(logger):
    try:
        from subprocess import DEVNULL  # Python 3.
    except ImportError:
        DEVNULL = open(os.devnull, 'wb')

    logger.info('---------- Accumulation Step Started ---------------------------')

    directory_out = settings.OUTPUT_DIRECTORY
    dem_in = directory_out + '/dem_filled.tif'

    logger.info('---------- Processing layers')
    dem_filled = 'dem_filled'
    grass.run_command('r.in.gdal', flags='o', input=dem_in, output=dem_filled, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    grass.run_command('g.proj', flags='c', georef=dem_in, stdout=DEVNULL, stderr=DEVNULL)
    grass.run_command('g.region', flags='sp', rast=dem_filled, stdout=DEVNULL, stderr=DEVNULL)

    accu_terra = 'accu_terra'
    grass.run_command('r.terraflow', elevation=dem_filled, accumulation=accu_terra, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    accu_w = 'accum_st'
    grass.run_command('r.watershed', elevation=dem_filled, accumulation=accu_w, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    streams = 'streams_st'
    grass.run_command('r.stream.extract', elevation=dem_filled, threshold=10000, accumulation=accu_w,
                      stream_raster=streams,
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    flowline = 'flowline'
    flowlength = 'flowlength'
    accu_flow = 'accum_flow'
    grass.run_command('r.flow', elevation=dem_filled, skip='4', flowaccumulation=accu_flow, flowline=flowline,
                      flowlength=flowlength, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # Save rasters
    logger.info('---------- Saving the rasters')
    raster_out = directory_out + '/' + 'accum_ho_terra.tif'
    grass.run_command('r.out.gdal', input=accu_terra, format='GTiff', createopt="PROFILE=GeoTIFF,COMPRESS=LZW",
                      output=raster_out, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    raster_out = directory_out + '/' + 'accum_ho_waters.tif'
    grass.run_command('r.out.gdal', input=accu_w, format='GTiff', createopt="PROFILE=GeoTIFF,COMPRESS=LZW",
                      output=raster_out, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    raster_out = directory_out + '/' + 'accum_ho_flow.tif'
    grass.run_command('r.out.gdal', input=accu_flow, format='GTiff', createopt="PROFILE=GeoTIFF,COMPRESS=LZW",
                      output=raster_out, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    hrus_shp = directory_out + '/flowline_ho.shp'
    grass.run_command('v.out.ogr', flags='c', input=flowline, format='ESRI_Shapefile', output=hrus_shp, type='line',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    raster_out = directory_out + '/' + 'flowlength_ho.tif'
    grass.run_command('r.out.gdal', input=flowlength, format='GTiff', createopt="PROFILE=GeoTIFF,COMPRESS=LZW",
                      output=raster_out, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    raster_out = directory_out + '/' + 'streams_ho.tif'
    grass.run_command('r.out.gdal', input=streams, format='GTiff', createopt="PROFILE=GeoTIFF,COMPRESS=LZW",
                      output=raster_out, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    logger.info('---------- Accumulation Step Ended   ---------------------------')
