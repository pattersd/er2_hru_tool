"""Provides optional install hooks for the application.

When an app is installed using the er2 installation script, you may provide logic in the
functions below to more easily build your app.
"""
import logging
import subprocess
import os

import er2.deploy.start as er2_start


HERE = os.path.abspath(os.path.dirname(os.path.abspath(__file__)))
STATIC = os.path.join(HERE, "static")


def post_install():
    """Runs after the repository has been deployed to its destination location"""
    install_npm_dependencies()
    link_er2_modules()
    update_permissions()
    log_build_commands()


def update_permissions():
    try:
        logging.getLogger("er2").info("Updating file permissions")
        args = ["chmod", "-R", "+x", os.path.join(STATIC, os.pardir, "bin")]
        output = subprocess.check_output(args)
        logging.getLogger("er2").debug(output)
    except subprocess.CalledProcessError as exc:
        logging.getLogger("er2").warn("Error updating file permissions:\n%s", exc.output)
        logging.getLogger("er2").warn("Please run the following manually (from %s): \n%s",
            STATIC, " ".join(args))


def install_npm_dependencies():
    """Installs npm dependencies via Yarn"""
    try:
        logging.getLogger("er2").info("Installing npm modules")
        args = (os.path.join(HERE, "static"))
        cmd = ("docker run -it --rm -v %s:/static/ erams/er2_web_build /bin/bash "
            "-c \"cd /static && yarn install\"" % args)
        er2_start.get_output(cmd)
    except subprocess.CalledProcessError as exc:
        logging.getLogger("er2").warn("Error installing npm modules:\n%s", exc.output)
        logging.getLogger("er2").warn("Please run the following manually: \n%s", cmd)


def link_er2_modules():
    """Link er2_map npm packages and add to node_modules using yarn"""
    try:
        logging.getLogger("er2").info("Linking er2_map packages")
        er2_apps_dir = os.path.join(HERE, '..')
        # er2_web_build directory paths
        app_name = os.path.basename(HERE)
        logging.getLogger("er2").info("app_name is {0}".format(app_name))
        er2_map_packages_dir = '/apps/er2_map/static/packages'
        er2_app_static_dir = '/apps/{app_name}/static'.format(app_name=app_name)
        for package in os.listdir(os.path.join(HERE, '../er2_map/static/packages')):
            package_yarn = package.replace("_", "-")
            # cd to the package dir to link it, then go the hru_tool static dir and add the package
            cmd = ("docker run -it --rm -v {apps_dir}:/apps/ erams/er2_web_build /bin/bash "
                   "-c \"cd {map_packages_dir}/{package}; yarn unlink; yarn link; cd {app_static_dir}/; yarn unlink {package_yarn}; yarn link {package_yarn}\""
                   .format(apps_dir=er2_apps_dir, map_packages_dir=er2_map_packages_dir, app_static_dir=er2_app_static_dir, package=package, package_yarn=package_yarn)
                   )
            output = subprocess.check_output(cmd, shell=True)
            logging.getLogger("er2").debug(output)
    except subprocess.CalledProcessError as exc:
        logging.getLogger("er2").warn("Error installing npm modules:\n%s", exc.output)
        logging.getLogger("er2").warn("Please run the following manually: \n%s", cmd)


def log_build_commands():
    """Helpful log message to help get new developers started building their new app."""
    msg = (" ".join(["\n\nCommand Reference:\n\nTo build your javascript, run:\n\n",
        "cd %s && ./bin/build" % HERE]))
    # Color these messages to stand out
    CSI = "\x1B["
    logging.getLogger("er2").info("%s %s %s", CSI+"0;92;40m", msg, CSI+"0m")