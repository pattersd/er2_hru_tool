REM Handy alias to build static assets

set DIR=%~dp0
cd %DIR%\..\static
docker run -it --rm -v %cd%:/static erams/er2_web_build /bin/bash -c "cd /static && npm run dev"
