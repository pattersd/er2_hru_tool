# -*- coding: utf-8 -*-
'''

 MODULE:       eRAMS2.0_HRU_delineation

 AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
               IRSTEA - France
               by Holm Kipka CSU

 PURPOSE:      Prepare files for the next steps of HRU Delineation
                   shapefile : selected gauges
                   rasters :   bounded DEM

'''

import logging
import math
import os
import re

import grass.script as grass

from er2.apps.er2_hru_tool import hru_tool_settings as settings


def find_dem_unit():
    try:
        from subprocess import DEVNULL  # Python 3.
    except ImportError:
        DEVNULL = open(os.devnull, 'wb')

    dem = settings.DEM_FILE
    unit = "m"
    info_list = os.popen("gdalsrsinfo -o proj4 %s" % dem).readline()
    # That is what the result looks like:
    # +proj=utm +zone=49 +datum=WGS84 +units=m +no_defs
    # +proj=utm +zone=13 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs
    # Now you just have to fine the UNIT in the string/list !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    # use regex to parse the unit
    match = re.search("\+units=([^\s]*)", info_list)
    if match is not None:
        unit = match.group(1)

    return unit;


#          km 1000.                Kilometer
#           m 1.                   Meter
#          dm 1/10                 Decimeter
#          cm 1/100                Centimeter
#          mm 1/1000               Millimeter
#         kmi 1852.0               International Nautical Mile
#          in 0.0254               International Inch
#          ft 0.3048               International Foot
#          yd 0.9144               International Yard
#          mi 1609.344             International Statute Mile
#        fath 1.8288               International Fathom
#          ch 20.1168              International Chain
#        link 0.201168             International Link
#       us-in 1./39.37             U.S. Surveyor's Inch
#       us-ft 0.304800609601219    U.S. Surveyor's Foot
#       us-yd 0.914401828803658    U.S. Surveyor's Yard
#       us-ch 20.11684023368047    U.S. Surveyor's Chain
#       us-mi 1609.347218694437    U.S. Surveyor's Statute Mile
#      ind-yd 0.91439523           Indian Yard
#      ind-ft 0.30479841           Indian Foot
#      ind-ch 20.11669506          Indian Chain
def convert_to_meters(value, unit):
    unit_table = {
        "km": 1000,
        "m": 1,
        "dm": 0.1,
        "cm": 0.01,
        "mm": 0.001,
        "kmi": 1852,
        "in": 0.0254,
        "ft": 0.3048,
        "yd": 0.9144,
        "mi": 1609.344,
        "fath": 1.8288,
        "ch": 20.1168,
        "link": 0.201168,
        "us-in": 1 / 39.37,
        "us-ft": 0.304800609601219,
        "us-yd": 0.914401828803658,
        "us-ch": 20.11684023368047,
        "us-mi": 1609.347218694437,
        "ind-yd": 0.91439523,
        "ind-ft": 0.30479841,
        "ind-ch": 20.11669506
    }

    factor = unit_table.get(unit)
    if factor is not None:
        value *= factor
    return value


def find_dem_resolution():
    try:
        from subprocess import DEVNULL  # Python 3.
    except ImportError:
        DEVNULL = open(os.devnull, 'wb')

    dem = settings.DEM_FILE
    dem_ch = 'dem_ch'
    outfile = open('/tmp/outfile.txt', 'w')
    outfile_err = open('/tmp/outfile_err.txt', 'w')
    grass.run_command('r.in.gdal', flags='o', input=dem, output=dem_ch, overwrite='True', stdout=outfile,
                      stderr=outfile_err)
    resolution = int(math.ceil(float(os.popen("r.info -g map=dem_ch").readlines()[4].split("=")[1])))
    return resolution;


def get_dem_resolution():
    logger = logging.getLogger(__name__)

    resolution = find_dem_resolution()
    unit = find_dem_unit()

    logger.info("Detected Resolution: %s%s" % (resolution, unit))

    if not (unit == 'm' or unit == 'km' or unit=='ft'):
        logger.info("Attempting to convert to meters...")
        res = convert_to_meters(resolution, unit)
        if res != resolution:
            resolution = res
            unit = "m"
        logger.info("Resolution: %s%s" % (resolution, unit))

    return {"resolution": resolution, "resolutionUnit": unit}


def fill_dem(resample_resolution, resolution_unit, logger):
    try:
        from subprocess import DEVNULL  # Python 3.
    except ImportError:
        DEVNULL = open(os.devnull, 'wb')
    logger.info('---------- Fill Dem Step Started ---------------------------')

    directory_out = settings.OUTPUT_DIRECTORY
    dem_name = settings.DEM_NAME
    dem = settings.DEM_FILE

    logger.info('---------- Preparing the layers')
    dem_ch = 'dem_ch'
    resolution = find_dem_resolution()
    logger.info('Detected resolution: %s' % (resolution))

    dem_corrected = dem

    # if resample resolution is zero then do no resample
    if resolution != resample_resolution:
        logger.info('---------- Resampling the DEM')
        dem_corrected = directory_out + '/' + dem_name + '_resampled.tif'
        os.system(
            'gdalwarp -overwrite -co "COMPRESS=LZW" -of GTiff -tr %s %s -r bilinear %s %s -dstnodata 999999 > /dev/null' % (
                resample_resolution, resample_resolution, dem, dem_corrected))
        grass.run_command('r.in.gdal', flags='o', input=dem_corrected, output=dem_ch, overwrite='True', stdout=DEVNULL,
                          stderr=DEVNULL)

    dem_in = dem_ch
    grass.run_command('g.proj', flags='c', georef=dem_corrected, stdout=DEVNULL, stderr=DEVNULL)
    grass.run_command('g.region', flags='p', raster=dem_in, stdout=DEVNULL, stderr=DEVNULL)

    logger.info('---------- Filling the DEM')
    dem_filled = 'dem_filled'
    dir_temp = 'dir_temp'
    unfilled_areas = 'unfilled_areas'

    areas = 1
    elev = dem_in
    while (areas > 0):
        grass.run_command('r.fill.dir', input=elev, output=dem_filled, direction=dir_temp, areas=unfilled_areas,
                          overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
        areas = int(grass.parse_command('r.info', flags='r', map=unfilled_areas, stdout=DEVNULL, stderr=DEVNULL)['max'])
        elev = dem_filled

    logger.info('---------- Saving the rasters')
    raster_out = directory_out + '/' + dem_name + '_filled.tif'
    grass.run_command('r.out.gdal', input=dem_filled, format='GTiff', createopt="PROFILE=GeoTIFF,COMPRESS=LZW",
                      output=raster_out, overwrite='True', stdout=DEVNULL, stderr=DEVNULL)
    logger.info('---------- Fill Dem Step Ended   ---------------------------')
