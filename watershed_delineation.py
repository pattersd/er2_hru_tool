# -*- coding: utf-8 -*-
'''

 MODULE:       eRAMS2.0_HRU_delineation

 AUTHOR(S):    adapted from GRASS-HRU (ILMS) - JENA University
               IRSTEA - France
               by Holm Kipka CSU

 PURPOSE:      Prepare files for the next steps of HRU Delineation
                   shapefile : selected gauges
                   rasters :   bounded DEM

'''
import logging
import string, subprocess, os, shutil, sys, glob, types
import math
import configparser
import grass.script as grass

from osgeo import gdal
from osgeo import ogr

from er2.apps.er2_hru_tool import hru_tool_settings as settings

def watershed_delineation(snap_distance, snap_distance_unit):
    try:
        from subprocess import DEVNULL  # Python 3.
    except ImportError:
        DEVNULL = open(os.devnull, 'wb')
    logger = logging.getLogger(__name__)

    logger.info('---------- Watershed Delineation Step Started ---------------------------')
    directory_out = settings.OUTPUT_DIRECTORY

    dem_in = directory_out + '/dem_filled.tif'
    # accu_in = directory_out + '/step3_accum_ho_flow.tif'
    # accu_in = directory_out + '/step3_accum_ho_terra.tif'
    accu_in = directory_out + '/accum_ho_waters.tif'
    streams_in = directory_out + '/streams_ho.tif'

    # Get the shape of gauges
    gauges_file = settings.OUTLET_FILE
    gauges_in = ogr.Open(gauges_file)
    gauges_lyr = gauges_in.GetLayer()

    logger.info('---------- Processing layers')
    dem_filled = 'dem_filled'
    grass.run_command('r.in.gdal', flags='o', input=dem_in, output=dem_filled, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)
    grass.run_command('g.proj', flags='c', georef=dem_in, stdout=DEVNULL, stderr=DEVNULL)
    grass.run_command('g.region', flags='sp', rast=dem_filled, stdout=DEVNULL, stderr=DEVNULL)

    accu = 'accu_w'
    grass.run_command('r.in.gdal', flags='o', input=accu_in, output=accu, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    streams = 'streams_w'
    grass.run_command('r.in.gdal', flags='o', input=streams_in, output=streams, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    in_outlet = 'outlet_v_map'
    grass.run_command('v.in.ogr', flags='r', input=gauges_file, output=in_outlet, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    snap_out = 'snap_out'
    grass.run_command('r.stream.snap', input=in_outlet, stream_rast=streams, accumulation=accu, radius=10,
                      output=snap_out,
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    outlet_shp = directory_out + '/snapped_point.shp'
    grass.run_command('v.out.ogr', flags='c', input=snap_out, format='ESRI_Shapefile', output=outlet_shp, type='point',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    drain = 'drain_wk'
    grass.run_command('r.watershed', elevation=dem_filled, drainage=drain, overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    ds = ogr.Open(outlet_shp)
    reloc_lyr = ds.GetLayer()
    nb_gauges = reloc_lyr.GetFeatureCount()

    i = 0
    grass.run_command('r.mapcalc', expression='basins = if((drain_wk >= 0), null(), null())', overwrite='True',
                      stdout=DEVNULL, stderr=DEVNULL)
    # Watersheds derivation
    basins = 'basins'
    rasters_list = 'basins'
    # r.cross doesn't accept more than 30 layers
    max_rasters = 29

    logger.info('---------- Derivating watersheds at gauges --')
    for gauge in reloc_lyr:
        geom = gauge.GetGeometryRef()
        gauge_x, gauge_y = geom.GetX(), geom.GetY()
        basin_wk = 'basin_wk'
        grass.run_command('r.water.outlet', input=drain, output='basin_tmp%d' % (i),
                          coordinates=str(gauge_x) + ',' + str(gauge_y), overwrite='True', stdout=DEVNULL,
                          stderr=DEVNULL)
        rasters_list += ',basin_tmp%d' % (i)
        i += 1
        if i % max_rasters == 0 or i == nb_gauges:
            grass.run_command('r.cross', input=rasters_list, output=basins, overwrite='True', stdout=DEVNULL,
                              stderr=DEVNULL)
            grass.run_command('g.remove', flags='f', type='raster', pattern='basin_tmp*', stdout=DEVNULL,
                              stderr=DEVNULL)
            rasters_list = 'basins'

    raster_out = directory_out + '/' + 'basin_ho_test.tif'
    grass.run_command('r.out.gdal', input=basins, output=raster_out, format='GTiff',
                      createopt="PROFILE=GeoTIFF,COMPRESS=LZW", overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    grass.run_command('g.region', raster=basins, stdout=DEVNULL, stderr=DEVNULL)
    basin_size = float(os.popen("r.surf.area map=%s units=hectares" % basins).readlines()[5].split(":")[1])
    if basin_size > 100:
        basin_size_km = basin_size / 100
        print(basin_size_km, " sq kilometers")
    else:
        print(basin_size, " hectares")

    vector_watershed = 'vec_shed'
    grass.run_command('r.to.vect', input=basins, output=vector_watershed, type='area', overwrite='True', stdout=DEVNULL,
                      stderr=DEVNULL)

    watershed_shp = directory_out + '/watershed_ho.shp'
    grass.run_command('v.out.ogr', flags='c', input=vector_watershed, format='ESRI_Shapefile', output=watershed_shp,
                      type='area', overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # Create mask = surface of all watersheds
    logger.info('---------- Creating the mask')
    mask_tmp = 'mask_tmp'
    cmd = 'echo "0 thru 10000000 = 1" > tmp/mask_rules.txt| r.reclass --overwrite input=' + basins + ' rules=tmp/mask_rules.txt output=' + mask_tmp
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    p.communicate()
    os.system("r.mapcalc 'mask_tmp = if(basins >= 0, 1, null())' --overwrite")
    grass.run_command('r.null', map=mask_tmp, null=0, setnull=1, stdout=DEVNULL, stderr=DEVNULL)
    grass.run_command('r.out.gdal', input=mask_tmp, type='UInt16', output=directory_out + '/' + 'mask.tif',
                      overwrite='True', stdout=DEVNULL, stderr=DEVNULL)

    # Save rasters
    logger.info('---------- Saving the rasters')

    logger.info('---------- Watershed Delineation Step Ended   ---------------------------')


