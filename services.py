"""er2_hru_tool classes and functions"""

import logging
import os
import subprocess
import threading

import er2.common.api as er2_api
import er2.common.config as er2_config
import er2.common.dict as er2_dict
import er2.common.state as er2_state
import er2.apps.er2_map.mapsource.mapsource as er2_mapsource
from er2.apps.er2_map.mapsource.mapsource import create_mapsource, mapsource_factory

from er2.apps.er2_map import services as svcs_map


def get_cwd():
    """Return the current working directory of this app"""
    return os.path.abspath(os.path.dirname(os.path.abspath(__file__)))


def load_config():
    """Load the application configuration.

    Returns:
        dict: application config as defined in config.yml
    """
    return er2_config.load_config(os.path.join(get_cwd(), "config.yml"))


def get_version():
    """Returns the current version of the application"""
    config = load_config()
    print(config)
    return load_config().get("version", "")


def get_app_name():
    """Returns the name of the application"""
    return load_config().get("name", "")


def get_api_resources():
    """Return a list of all API resources associated with the app.

    Used by eRAMS to populate a self-describing API at /api

    Returns:
        A list of api resources
    """
    import er2.common.urls as er2_urls
    import er2.apps.er2_hru_tool.urls as app_urls
    api_urls = []
    for url, prefix in er2_urls.walk(app_urls.urlpatterns, get_app_name()):
        if "api" in url.regex.pattern.split("/"):
            meta = {"selfLink": er2_urls.prettify("/"+"/".join([prefix, url.regex.pattern]))}
            api_urls.append(er2_api.ApiResource(version=get_version(), meta=meta))
    return [dict(url) for url in api_urls]

class State(er2_state.State):
    """Represents the state of an er2_hru_tool application"""

    def __init__(self, props=None, request=None, version=get_version(), meta={}):
        super(State, self).__init__(props, request, version, meta)

    def get_state(self):
        state = super().get_state()
        # TODO: move this out of the service and just make it its own class
        # Map state object uses the token to determine the user layer directory,
        map_svc = svcs_map.State(request=self._request)
        # override the possibly different token with the main token
        map_svc._token = self._token
        map_state = map_svc.get_state()

        # Register HRU tools with mapserver by linking the /files directory
        #   to the /mapserver/<token> directory
        d = map_svc.get_mapsource_dir()
        mapsource_dir = os.path.join(d, 'files')
        if not os.path.exists(mapsource_dir):
            os.symlink('/files', os.path.join(mapsource_dir))

            # Add HRU mapsource
            hru_mapsource = mapsource_factory(os.path.join(mapsource_dir, 'files.mapsource'))
            if not hru_mapsource:
                # TODO: make er2_map service usage consistent
                hru_mapsource = map_svc.create_mapsource('mapserver', mapsource_name='files')
        state.update(map_state)
        return state

    def merge_state(self, new_state):
        """Combines cached state with a new incoming state.

        Also, removes unncessary properties or performs actions on the state based on
        logic specific to the er2_hru_tool application.

        Arguments:
            new_state (dict): A subset of application state coming from the client.
        Returns:
            (dict): A new dictionary with a correct representation of the application
                    state after the `new_state` is applied
        """
        merged_state = {"token": self._token}
        cached_state = self.get_state()
        for key in set(list(new_state.keys()) + list(cached_state.keys())):
            # Perform a standard merge for all keys
            # Optionally, add logic to manually omit or merge keys differently.
            if key in cached_state and key in new_state:
                merged_state[key] = er2_dict.update(cached_state[key], new_state[key])
            elif key in cached_state and key not in new_state:
                merged_state[key] = cached_state[key]
            elif key not in cached_state and key in new_state:
                merged_state[key] = new_state[key]

        logging.getLogger("er2").debug(merged_state)
        return merged_state

    def PATCH(self, *args, **kwargs):
        self.state = self.set_state(self.merge_state(er2_api.load_json(self._request.body)))
        return er2_api.respond(self)

class ProjectState(State):
    """Represents the state of an er2_hru_tool project"""

    def __init__(self, props=None, request=None):
        super(ProjectState, self).__init__(props, request)

    def GET(self, *args, **kwargs):
        if not hasattr(self, 'state'):
            self.state = self.get_state()
        return er2_api.respond(self)

    def run(self, project_path):
        self.state = self.get_state()
        runnable = '/ages/ages-demo/ages.sh'
        thread = RunProjectThread(runnable, project_path)
        thread.start()
        thread.join()

        self.state.update({ 'results': thread.get_results() })
        self.set_state(self.state)
        return self


class RunProjectThread(threading.Thread):
    def __init__(self, runnable, project_path):
        super().__init__()

        self.runnable = runnable
        self.project_path = os.path.basename(project_path)

        assert os.path.splitext(self.runnable)[1] == '.sh', 'Invalid project file'
        os.chmod(self.runnable, 0o777)
        self.cwd = os.path.dirname(self.runnable)
        self.logfile_path = os.path.join(self.cwd, 'log.txt')

    def run(self):
        logfile = open(self.logfile_path, 'w')
        out, err = subprocess.Popen(['{0} {1}'.format(self.runnable, self.project_path)], shell=True, cwd=self.cwd, stdout=logfile).communicate()

    def get_results(self):
        logfile = open(self.logfile_path)
        return logfile.read()
