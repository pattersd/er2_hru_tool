#!/usr/bin/env bash

nohup redis-server &
service apache2 start
echo "You may now connect to http://localhost to run hru_tool"
tail -f /dev/null
