import argparse
import jinja2
import logging
import os
import subprocess


def setup(args, debug=False):
    """Prepare all settings, packages and shared objects for deployment"""
    logger = logging.getLogger("er2")
    logger.setLevel(logging.DEBUG if debug else logging.INFO)
    console_handler = logging.StreamHandler()
    # formatter = logging.Formatter("%(asctime)s %(levelname)s %(message)s")
    # console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)


def run():
    mapserver_peers = os.getenv("MAPSERVER_PEERS", "")
    if len(mapserver_peers):
        mapserver_peers = mapserver_peers.split(",")
    else:
        mapserver_peers = []
    logging.getLogger("er2").info("MAPSERVER_PEERS: %s", mapserver_peers)
    defaults = {"mapserver_peers": mapserver_peers}
    loader = jinja2.Environment(loader=jinja2.FileSystemLoader('/scripts/templates'))
    template = loader.get_template('haproxy.cfg')
    with open("/etc/haproxy/haproxy.cfg", "w") as env_file:
        env_file.write(template.render(**defaults))


def teardown():
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate HAProxy configuration for FICUS applications")
    parser.add_argument("-d", "--debug", action="store_true", help="Enable debug output",
                        default=False)
    args = parser.parse_args()

    try:
        setup(args.debug)
        run()
    except subprocess.CalledProcessError as e:
        logging.getLogger("er2").warn("Could not generate haproxy configuration: %s", e)
    finally:
        teardown()
