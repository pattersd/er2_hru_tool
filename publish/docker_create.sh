#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo Create docker image for hru_tool. Pass 'true' as first argument to update dockerhub.
    echo For example to tag the current version as 0.5 and stable, use:
    echo   $0 true 0.5 latest
    echo To tag the current version as beta use:
    echo   $0 true
    echo "Arguments: $0 <update_repository> <tag1> <tag2> ... <tagn>"
    exit
fi

# pass in 'true' to publish the images
update_repository=$1
shift

# Copy django app files and static assets
app=`readlink -f ../../../..`
echo app dir is $app

staging_dir=/tmp/app
rm -rf $staging_dir
mkdir $staging_dir
mkdir $staging_dir/er2
mkdir $staging_dir/er2/apps

echo Copying apps to $staging_dir
cp $app/*.* $staging_dir
cp $app/er2/*.py $staging_dir/er2

for sub_app in $app/er2/apps/*; do
    if [ -d $sub_app ] ; then
        app_name=`basename $sub_app`
        mkdir $staging_dir/er2/apps/$app_name

        # django
        cp $sub_app/*.* $staging_dir/er2/apps/$app_name

        # Copy static
        if [ -d $sub_app/static/$app_name ] ; then
            mkdir $staging_dir/er2/apps/$app_name/static
            echo Copying $sub_app/static/$app_name to $staging_dir/er2/apps/$app_name/static
            cp -r $sub_app/static/$app_name $staging_dir/er2/apps/$app_name/static
        fi

        # Copy subdirs
        for subdir in $sub_app/*; do
            if [ -d $subdir ] && [ `basename $subdir` != "static" ] ; then
                cp -r $subdir $staging_dir/er2/apps/$app_name
            fi
        done
    fi
done

app_name=er2_hru_tool

# common code
for pydir in $app/er2/common $app/er2/auth; do
    echo $pydir
    if [ -d $pydir ]; then
        pybase=`basename $pydir`
        echo Creating app/er2/$pybase
        mkdir $staging_dir/er2/$pybase
        cp $pydir/*.* $staging_dir/er2/$pybase
    fi
done

chmod -R gou+rX $staging_dir/er2
echo Copying $staging_dir to $app_name
mv $staging_dir $app_name

export VERSION=0.0.1
echo Version is $VERSION
sed -i -e "s/VERSION=\".*\"/VERSION=\"${VERSION}\"/g" $app_name/Dockerfile
docker-compose down
docker-compose build

if [ "$update_repository" = "true" ]; then
    docker push omslab/$app_name:$VERSION
fi

# Add additional tags and push if requested.
for tag in $@; do
    docker tag omslab/$app_name:$VERSION omslab/$app_name:$tag
    if [ "$update_repository" = "true" ]; then
        docker push omslab/$app_name:$tag
    fi
done

rm -rf $app_name/app

docker run -it --rm -p 80:80 -p 8000:8000 -v /home/dave/work/er2_web_hru_tool/app/er2/apps/er2_hru_tool/files:/files omslab/${app_name}:$VERSION
