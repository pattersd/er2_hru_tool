#!/bin/bash

if [ "$1" = "-h" ]; then
    echo "Create er2_web codebase from scratch in staging directory"
    echo "  and generate docker image. Pass -use_er2_web_utils to run yarn and webpack from the er2_web_utils container."
    echo   "(otherwise local versions of yarn and webpack will be used)."
    echo "  Use -push to push the container to docker hub."
    echo "Arguments: $0 <-use_er2_web_utils> <-t tag1> <-t tag2> <-name name> <-push> ... <er2_repository_url>"
    exit
fi

# Load from environment
if [ -z $use_er2_web_utils ]; then
    use_er2_web_utils="false"
fi
if [ -z $push_to_dockerhub ]; then
    push_to_dockerhub="false"
fi
if [ -z $tags ]; then
    tags=()
fi
# default container name is set below app_name

# process command line
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -use_er2_web_utils)
    use_er2_web_utils="true"
    shift
    ;;
    -container_name)
    shift
    name=$1
    shift
    ;;
    -h)
    shift
    ;;
    -push)
    push_to_dockerhub="true"
    shift
    ;;
    -t)
    shift
    tags="$tags $1"
    shift
    ;;
    *)
    er2_repository=$key
    shift
    ;;
esac
done

echo Tags = ${tags}
echo repo = ${er2_repository}

staging_repo=staging
if [ -d $staging_repo ]; then
    #echo "Skipping repo delete"
    rm -rf $staging_repo
fi

echo Creating repository in $staging_repo...
#hg clone https://${username}@alm.engr.colostate.edu/cb/hg/er2_web $staging_repo
# FOr now use pattersd's bitbucket version until python3 install problem is fixed
hg clone https://pattersd@bitbucket.org/pattersd/er2_web $staging_repo
cd $staging_repo
staging_repo_path=`pwd`

# Create virtual env installation
echo Creating virtual environment for python in $staging_repo/venv_$er2_repository
python3 -m venv venv
source venv/bin/activate
pip install --upgrade pip

cd app/er2/deploy
# TODO: need to add repository type argument for repos like er2_hru_tool
python install.py $er2_repository

# Because the repository name may be different from the app name, look for it in the apps dir
app_name=""
cd ../apps
for i in er2_*; do
    # for now just assume anything that is not er2_apo
    if [[ $er2_repository == *"$i"* ]]; then
        app_name=$i
    fi
done

// Set container name from app_name if not set in env.
if [ -z $container_name ]; then
    if [ -z $name ]; then
        # fallback to app name
        container_name=$app_name
    else
        # set from command line
        container_name=$name
    fi
fi

cd ../deploy
# create settings.py
python start.py --norun

# Next step is to run webpack to build client access.
cd $staging_repo_path/app/er2/apps/$app_name/static

if [ $use_er2_web_utils == 'true' ]; then
    docker run -it --rm -v `pwd`:/static erams/er2_web_build /bin/bash -c "cd /static/ && yarn" er2_web_utils
else
    yarn
fi

if [ $use_er2_web_utils == 'true' ]; then
    docker run -it --rm -v `pwd`:/static erams/er2_web_build /bin/bash -c "cd /static/ && npm run build" er2_web_utils
else
    npm run build
fi

# build websocket static files
if [ $use_er2_web_utils == 'true' ]; then
    docker run -it --rm -v `pwd`:/static erams/er2_web_build /bin/bash -c "cd /static/er2_ficus/js/modules/websockets/server && yarn" er2_web_utils
else
    (cd er2_ficus/js/modules/websockets/server; yarn)
fi

#### Create docker image, tag it, and push to dockerhum.

# Copy django app files and static assets
#    cwd should be er2/apps/<er2_proj>
app_root=`readlink -f ../../../..`
if [ ! -d $app_root/er2 ]; then
    echo "Problem with current working directory"
    exit
fi
echo app dir is $app

staging_dir=`mktemp -d`
mkdir $staging_dir/er2
mkdir $staging_dir/er2/apps

echo "Building staging dir $staging_dir"
cp $app_root/* $staging_dir
cp $app_root/er2/*.py $staging_dir/er2

for app in $app_root/er2/apps/*; do
    if [ -d $app ] ; then
        base_app_name=`basename $app`
        staging_app_dir=$staging_dir/er2/apps/$base_app_name

        echo "Copying app $app to $staging_app_dir"
        mkdir $staging_app_dir
        cp $app/*.* $staging_app_dir

        # Copy app code leaving out node_modules
        if [ -d $app/static/$base_app_name ] ; then
            mkdir $staging_app_dir/static
            echo Copying $app/static/$base_app_name to $staging_app_dir/static
            cp -r $app/static/$base_app_name $staging_app_dir/static
        fi

        # Copy app subdirs
        for subdir in $app/*; do
            if [ -d $subdir ] && [ `basename $subdir` != "static" ] ; then
                cp -r $subdir $staging_app_dir
            fi
        done
    fi
done

# common code. Not sure if auth needs to be copied?
for pydir in $app_root/er2/common; do
    pybase=`basename $pydir`
    echo Creating app/er2/$pybase
    mkdir $staging_dir/er2/$pybase
    cp $pydir/*.* $staging_dir/er2/$pybase
done

chmod -R gou+rX $staging_dir/er2
# change to "publish" directory containing the dockerfile setup.
cd ../publish
echo PWD is `pwd`
mv $staging_dir $app_name/app

export VERSION=`hg parents --template '{latesttag}'`
echo Version is $VERSION
sed -i -e "s/VERSION=\".*\"/VERSION=\"${VERSION}\"/g" er2_ficus/Dockerfile
# Create base image.
docker build -t ${app_name}_base ${app_name}_base
# Update with latest app code
docker build -t $app_name $app_name
docker tag $app_name $container_name:$VERSION

if [ "$push_to_dockerhub" == "true" ]; then
    docker push $container_name:$VERSION
fi

# Add additional tags and push if requested.
for tag in $tags; do
    echo "Adding tag $tag"
    target_tag=$container_name:$tag
    docker tag $app_name $target_tag
    if [ "$push_to_dockerhub" == "true" ]; then
        docker push $target_tag
    fi
done

# cleanup
#rm -rf app
