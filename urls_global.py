"""All urls contained in this file will be appended to the global URL namespace.

For instance, if a url pattern of the form:

django.conf.urls.url(r'$', views.index)

Is included as a url pattern, the root URL "/" will be routed to er2_hru_tool.views.index
"""
import django.conf.urls

from er2.apps.er2_hru_tool import views

urlpatterns = [
    django.conf.urls.url(r'$', views.index),
]
